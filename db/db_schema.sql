CREATE DATABASE  IF NOT EXISTS `russchat`; 
USE `russchat`;

DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `attachmentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `attachmentName` varchar(45) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `wallMessageId` bigint(20) NOT NULL,
  PRIMARY KEY (`attachmentId`),
  UNIQUE KEY `attachmentId_UNIQUE` (`attachmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `comment_history`;
CREATE TABLE `comment_history` (
  `commentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `wallMessageId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `commentText` text NOT NULL,
  PRIMARY KEY (`commentId`),
  UNIQUE KEY `commentId_UNIQUE` (`commentId`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `conversation`;
CREATE TABLE `conversation` (
  `conversationId` bigint(20) NOT NULL AUTO_INCREMENT,
  `conversationName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`conversationId`),
  UNIQUE KEY `conversationId_UNIQUE` (`conversationId`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `conversation_user`;
CREATE TABLE `conversation_user` (
  `conversationUserId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `conversationId` bigint(20) NOT NULL,
  PRIMARY KEY (`conversationUserId`),
  UNIQUE KEY `conversationUserId_UNIQUE` (`conversationUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `like_history`;
CREATE TABLE `like_history` (
  `likeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `wallMessageId` bigint(20) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`likeId`),
  UNIQUE KEY `likeId_UNIQUE` (`likeId`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `messageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `messageText` longtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `conversationId` bigint(20) NOT NULL,
  PRIMARY KEY (`messageId`),
  UNIQUE KEY `messageId_UNIQUE` (`messageId`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `received_comment_history`;
CREATE TABLE `received_comment_history` (
  `receivedCommentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `commentId` bigint(20) NOT NULL,
  PRIMARY KEY (`receivedCommentId`),
  UNIQUE KEY `receivedCommentId_UNIQUE` (`receivedCommentId`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `received_like_history`;
CREATE TABLE `received_like_history` (
  `receivedLikeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `likeId` bigint(20) NOT NULL,
  PRIMARY KEY (`receivedLikeId`),
  UNIQUE KEY `receivedLikeId_UNIQUE` (`receivedLikeId`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `received_message`;
CREATE TABLE `received_message` (
  `receivedMessageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `messageId` bigint(20) NOT NULL,
  PRIMARY KEY (`receivedMessageId`),
  UNIQUE KEY `receivedMessageId_UNIQUE` (`receivedMessageId`)
) ENGINE=InnoDB AUTO_INCREMENT=634 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `received_wall_message`;
CREATE TABLE `received_wall_message` (
  `receivedWallMessageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `wallMessageId` bigint(20) NOT NULL,
  PRIMARY KEY (`receivedWallMessageId`),
  UNIQUE KEY `receivedWallMessageId_UNIQUE` (`receivedWallMessageId`)
) ENGINE=InnoDB AUTO_INCREMENT=458 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `schoolId` int(11) NOT NULL AUTO_INCREMENT,
  `schoolName` varchar(145) NOT NULL,
  `country` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip` varchar(45) NOT NULL,
  `address` varchar(145) NOT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`schoolId`),
  UNIQUE KEY `schoolId_UNIQUE` (`schoolId`),
  UNIQUE KEY `schoolName_UNIQUE` (`schoolName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `school_class`;
CREATE TABLE `school_class` (
  `schoolClassId` int(11) NOT NULL AUTO_INCREMENT,
  `className` varchar(145) NOT NULL,
  `schoolId` int(11) NOT NULL,
  PRIMARY KEY (`schoolClassId`),
  UNIQUE KEY `schoolClassId_UNIQUE` (`schoolClassId`),
  UNIQUE KEY `className_UNIQUE` (`className`),
  KEY `school_class_idx` (`schoolId`),
  CONSTRAINT `school_class` FOREIGN KEY (`schoolId`) REFERENCES `school` (`schoolId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `unreceived_comment_history`;
CREATE TABLE `unreceived_comment_history` (
  `unReceivedCommentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `commentId` bigint(20) NOT NULL,
  PRIMARY KEY (`unReceivedCommentId`),
  UNIQUE KEY `unReceivedCommentId_UNIQUE` (`unReceivedCommentId`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `unreceived_like_history`;
CREATE TABLE `unreceived_like_history` (
  `unReceivedLikeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `likeId` bigint(20) NOT NULL,
  PRIMARY KEY (`unReceivedLikeId`),
  UNIQUE KEY `unReceivedLikeId_UNIQUE` (`unReceivedLikeId`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `unreceived_wall_post_history`;
CREATE TABLE `unreceived_wall_post_history` (
  `unReceivedWallPostId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `wallMessageId` bigint(20) NOT NULL,
  PRIMARY KEY (`unReceivedWallPostId`),
  UNIQUE KEY `unReceivedWallPostId_UNIQUE` (`unReceivedWallPostId`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `loginId` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userTypeId` int(11) NOT NULL,
  `profileIcon` varchar(200) DEFAULT NULL,
  `schoolClass` int(11) DEFAULT NULL,
  `colour` int(11) NOT NULL DEFAULT '0',
  `nickName` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `fullName` varchar(150) DEFAULT NULL,
  `birthdate` varchar(10) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `uid_UNIQUE` (`userId`),
  UNIQUE KEY `loginid_UNIQUE` (`loginId`),
  KEY `user_school_class_idx` (`schoolClass`),
  CONSTRAINT `user_school_class` FOREIGN KEY (`schoolClass`) REFERENCES `school_class` (`schoolClassId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `userTypeId` int(11) NOT NULL,
  `userTypeName` varchar(45) NOT NULL,
  PRIMARY KEY (`userTypeId`),
  UNIQUE KEY `userTypeId_UNIQUE` (`userTypeId`),
  UNIQUE KEY `userTypeName_UNIQUE` (`userTypeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wall`;
CREATE TABLE `wall` (
  `wallId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`wallId`),
  UNIQUE KEY `wallId_UNIQUE` (`wallId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wall_message`;
CREATE TABLE `wall_message` (
  `wallMessageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `messageText` longtext,
  `userId` bigint(20) NOT NULL,
  `wallId` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wallMessageId`),
  UNIQUE KEY `wallMessageId_UNIQUE` (`wallMessageId`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wall_user`;
CREATE TABLE `wall_user` (
  `wallUserId` bigint(20) NOT NULL AUTO_INCREMENT,
  `wallId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`wallUserId`),
  UNIQUE KEY `wallUserId_UNIQUE` (`wallUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;