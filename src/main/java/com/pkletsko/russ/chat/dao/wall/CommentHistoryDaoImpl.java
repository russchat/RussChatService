package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.wall.CommentHistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CommentHistoryDaoImpl extends AbstractDao implements CommentHistoryDao {

    static Log log = LogFactory.getLog(CommentHistoryDaoImpl.class.getName());

	@Override
	public Long addCommentHistory(final CommentHistory commentHistory) throws Exception {
        if (commentHistory == null) {
             return null;
        }

        Long commentHistoryId = (Long) getSession().save(commentHistory);
        log.debug("comment history id : " + commentHistoryId + " was added.");

        return commentHistoryId;
	}

    @Override
    public boolean updateCommentHistory(final CommentHistory commentHistory) throws Exception {
        if (commentHistory == null) {
            return false;
        }

        getSession().saveOrUpdate(commentHistory);
        log.debug("comment history id : " + commentHistory.getCommentId() + " was updated.");

        return true;
    }

	@Override
	public CommentHistory getCommentHistoryById(final Long commentHistoryId) throws Exception {
        if (commentHistoryId == null) {
            return null;
        }

        return (CommentHistory) getSession().get(CommentHistory.class, commentHistoryId);
	}
}
