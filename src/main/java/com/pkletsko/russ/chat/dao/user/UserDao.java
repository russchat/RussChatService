package com.pkletsko.russ.chat.dao.user;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.WallMessage;

import java.util.List;
import java.util.Set;

public interface UserDao {

	 User addUser(final User user) throws Exception;

     boolean updateUser(final User user) throws Exception;

     boolean deleteUser(final Long userId) throws Exception;

	 User getUserById(final Long userId) throws Exception;

     User getUserByLoginId(final String id) throws Exception;

     List<User> search(final String searchString);

     Set<WallMessage> getReceivedWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception;

     Set<WallMessage> getWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception;

    Set<User> getFollowing(Long userId, Long lastLoadedUserId, int itemsPerPage, int direction);

    Set<User> getFollowers(Long userId, Long lastLoadedUserId, int itemsPerPage, int direction);

    Set<User> getAllFollowing(Long userId);
}
