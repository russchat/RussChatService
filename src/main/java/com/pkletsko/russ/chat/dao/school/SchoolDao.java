package com.pkletsko.russ.chat.dao.school;

import com.pkletsko.russ.chat.model.school.School;
import com.pkletsko.russ.chat.model.school.SchoolClass;

import java.util.List;

public interface SchoolDao {

    public boolean addSchool(final School school) throws Exception;

    public boolean addSchoolClass(final SchoolClass schoolClass) throws Exception;

    public School getSchoolById(final int schoolId) throws Exception;

    public SchoolClass getSchoolClassById(final int schoolClassId) throws Exception;

    public List<School> getSchoolList() throws Exception;

    public List<School> search(final String searchString);

    public List<SchoolClass> searchSchoolClass(final String searchString);
}
