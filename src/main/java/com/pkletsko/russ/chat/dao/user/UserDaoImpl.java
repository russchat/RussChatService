package com.pkletsko.russ.chat.dao.user;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.Wall;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserDaoImpl extends AbstractDao implements UserDao {

    private final int UP = 0;
    private final int DOWN = 1;

    static Log log = LogFactory.getLog(UserDaoImpl.class.getName());

    /**
     * Add new user to database
     *
     * @param user
     * @return
     * @throws Exception
     */
    @Override
    public User addUser(final User user) throws Exception {
        if (user == null) {
            return null;
        }

        Long userId = (Long) getSession().save(user);
        log.debug("user id : " + userId + " was added.");

        return getUserById(userId);
    }

    /**
     * Update User in database
     *
     * @param user
     * @return
     * @throws Exception
     */
    @Override
    public boolean updateUser(final User user) throws Exception {
        if (user == null) {
            return false;
        }

        getSession().update(user);
        log.debug("wall id : " + user.getUserId() + " was updated.");

        return true;
    }

    /**
     * @param userId
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean deleteUser(final Long userId) throws Exception {
        log.debug("delete user id: " + userId);
        if (userId == null) {
            return false;
        }

        User user = getUserById(userId);
        if (user != null) {
            getSession().delete(user);
        }
        return false;
    }

    /**
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public User getUserById(final Long userId) throws Exception {
        if (userId == null) {
            return null;
        }

        return (User) getSession().get(User.class, userId);
    }

    @Override
    public Set<WallMessage> getWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception {
        if (userId == null) {
            return null;
        }
        Criteria cr = getSession().createCriteria(WallMessage.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("messageId"));
        } else {
            cr.addOrder(Order.desc("messageId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (fromWallPostId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("messageId", fromWallPostId));
            } else {
                cr.add(Restrictions.lt("messageId", fromWallPostId));
            }
        }
        cr.add(Restrictions.eq("ownerOfWallMessage.userId", userId));
        cr.createCriteria("ownerOfWallMessage").add(Restrictions.like("userId", userId));

        Set<WallMessage> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }

    @Override
    public Set<WallMessage> getReceivedWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception {
        if (userId == null) {
            return null;
        }

        Criteria cr = getSession().createCriteria(WallMessage.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("messageId"));
        } else {
            cr.addOrder(Order.desc("messageId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (fromWallPostId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("messageId", fromWallPostId));
            } else {
                cr.add(Restrictions.lt("messageId", fromWallPostId));
            }
        }
        cr.add(Restrictions.not(Restrictions.eq("ownerOfWallMessage.userId", userId)));
        cr.createCriteria("usersReceivedTheWallMsg").add(Restrictions.like("userId", userId));
                //TODO remove test code
//        getSession().createCriteria(WallMessage.class)
//                .addOrder(Order.asc("messageId"))
//                .setMaxResults(5)
//                .add(Restrictions.gt("messageId", 0L))
//                .add(Restrictions.not(Restrictions.eq("ownerOfWallMessage.userId", userId)))
//                .createCriteria("usersReceivedTheWallMsg")
//                .add(Restrictions.like("userId", userId))
//                .list();
//
//        getSession().createCriteria(WallMessage.class)
//                .addOrder(Order.desc("messageId"))
//                .setMaxResults(5)
//                .add(Restrictions.lt("messageId", 0L))
//                .add(Restrictions.not(Restrictions.eq("ownerOfWallMessage.userId", userId)))
//                .createCriteria("usersReceivedTheWallMsg")
//                .add(Restrictions.like("userId", userId))
//                .list();

        Set<WallMessage> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }

    @Override
    public Set<User> getAllFollowing(Long userId) {
        if (userId == null) {
            return null;
        }

        Criteria cr = getSession().createCriteria(User.class);

        cr.add(Restrictions.not(Restrictions.eq("userId", userId)));

        cr.createCriteria("myWalls").createCriteria("followingUsers").add(Restrictions.like("userId", userId));

        Set<User> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }


    @Override
    public Set<User> getFollowing(Long userId, Long lastLoadedUserId, int itemsPerPage, int direction) {
        if (userId == null) {
            return null;
        }

//        //TODO TEST
//        getSession().createCriteria(User.class).addOrder(Order.desc("userId")).setMaxResults(itemsPerPage).add(Restrictions.gt("userId", lastLoadedUserId)).createCriteria("myWalls").createCriteria("followingUsers").add(Restrictions.like("userId", userId)).list()
//                .createCriteria("myWalls")
//                .createCriteria("followingUsers")
//                .add(Restrictions.like("userId", userId))
//                .list();

        Criteria cr = getSession().createCriteria(User.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("userId"));
        } else {
            cr.addOrder(Order.desc("userId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (lastLoadedUserId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("userId", lastLoadedUserId));
            } else {
                cr.add(Restrictions.lt("userId", lastLoadedUserId));
            }
        }

        cr.add(Restrictions.not(Restrictions.eq("userId", userId)));

        cr.createCriteria("myWalls").createCriteria("followingUsers").add(Restrictions.like("userId", userId));

        Set<User> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }

    @Override
    public Set<User> getFollowers(Long userId, Long lastLoadedUserId, int itemsPerPage, int direction) {

        if (userId == null) {
            return null;
        }

        Criteria cr = getSession().createCriteria(User.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("userId"));
        } else {
            cr.addOrder(Order.desc("userId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (lastLoadedUserId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("userId", lastLoadedUserId));
            } else {
                cr.add(Restrictions.lt("userId", lastLoadedUserId));
            }
        }

        cr.add(Restrictions.not(Restrictions.eq("userId", userId)));
        cr.createCriteria("walls").add(Restrictions.eq("ownerOfWall.userId", userId));

        Set<User> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }

    /**
     * @param loginId
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public User getUserByLoginId(final String loginId) throws Exception {
        if (loginId == null || loginId.isEmpty()) {
            return null;
        }

        //find requested user by loginId
        List<User> list = getSession().createQuery("from User where loginid=?").setParameter(0, loginId).list();

        // this userId is not existed in database
        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<User> search(final String searchString) {


        BooleanQuery bQuery = new BooleanQuery();
        Session session = getSession();

        FullTextSession fullTextSession = Search.getFullTextSession(session);

        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Analyzer analyzer = fullTextSession.getSearchFactory().getAnalyzer("searchtokenanalyzer");

        QueryParser parser = new QueryParser("fullName", analyzer);

        String[] tokenized = null;

        try {
            Query query = parser.parse(searchString);
            String cleanedText = query.toString("fullName");
            tokenized = cleanedText.split("\\s");


        } catch (org.apache.lucene.queryparser.classic.ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        QueryBuilder qBuilder = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(User.class).get();

        for (int i = 0; i < tokenized.length; i++) {
            if (i == (tokenized.length - 1)) {
                Query query = qBuilder.keyword().wildcard().onField("fullName")
                        .matching(tokenized[i] + "*").createQuery();
                bQuery.add(query, BooleanClause.Occur.MUST);
            } else {
                Term exactTerm = new Term("fullName", tokenized[i]);
                bQuery.add(new TermQuery(exactTerm), BooleanClause.Occur.MUST);
            }
        }

//        for (User exGoal : existingGoals) {
//            Term omittedTerm = new Term("id", String.valueOf(exGoal.getUserId()));
//            bQuery.add(new TermQuery(omittedTerm), BooleanClause.Occur.MUST_NOT);
//        }

        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bQuery, User.class);

        List<User> results = hibQuery.list();

        return results;
    }

}