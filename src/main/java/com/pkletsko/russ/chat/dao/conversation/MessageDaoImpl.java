package com.pkletsko.russ.chat.dao.conversation;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.conversation.ConversationMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.Set;

public class MessageDaoImpl extends AbstractDao implements MessageDao {

    static Log log = LogFactory.getLog(MessageDaoImpl.class.getName());

    private final int UP = 0;
    private final int DOWN = 1;

	@Override
	public Long addMessage(final ConversationMessage message) throws Exception {
        if (message == null) {
            return null;
        }

        Long messageId = (Long) getSession().save(message);
        log.debug("a new message : " + messageId);

		return messageId;
	}

    @Override
    public boolean updatedMessage(final ConversationMessage message) throws Exception {
        if (message == null) {
            return false;
        }

        getSession().saveOrUpdate(message);
        log.debug("message id : " + message.getMessageId() + " was updated.");
        return true;
    }

    @Override
	public ConversationMessage getMessageById(final Long messageId) throws Exception {
        if (messageId == null) {
            return null;
        }
        return (ConversationMessage) getSession().get(ConversationMessage.class, messageId);
	}

    @Override
    public Set<ConversationMessage> getConversationMessages(Long conversationId, Long lastLoadedMsgId, int itemsPerPage, Integer direction) {
        Criteria cr = getSession().createCriteria(ConversationMessage.class);

        if (direction == UP) {
            cr.addOrder(Order.desc("messageId"));
        } else {
            cr.addOrder(Order.asc("messageId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (lastLoadedMsgId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.lt("messageId", lastLoadedMsgId));
            } else {
                cr.add(Restrictions.gt("messageId", lastLoadedMsgId));
            }
        }
        cr.add(Restrictions.eq("conversation.conversationId", conversationId));

        Set<ConversationMessage> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }
}
