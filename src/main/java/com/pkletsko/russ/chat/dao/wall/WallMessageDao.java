package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.model.wall.CommentHistory;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;

import java.util.Set;

public interface WallMessageDao {

    WallMessage addWallMessage(final WallMessage wallMessage) throws Exception;

    boolean updateWallMessage(final WallMessage wallMessage) throws Exception;

    WallMessage getWallMessageById(final Long wallMessageId) throws Exception;

    Set<CommentHistory> getComments(final Long wallPostId, final Long lastLoadedCommentId, int itemsPerPage, int direction) throws Exception;

    Set<LikeHistory> getLikes(final Long wallPostId, final Long lastLoadedLikeId, int itemsPerPage, int direction) throws Exception;

}
