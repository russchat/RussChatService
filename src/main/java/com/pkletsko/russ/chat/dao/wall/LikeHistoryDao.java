package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.model.wall.LikeHistory;

public interface LikeHistoryDao {

    Long addLikeHistory(final LikeHistory likeHistory) throws Exception;

    boolean updateLikeHistory(final LikeHistory likeHistory) throws Exception;

    boolean deleteLikeHistory(final Long likeHistoryId) throws Exception;

    LikeHistory getLikeHistoryById(final Long likeHistoryId) throws Exception;

    LikeHistory getLikeHistory(final Long userId, final Long wallPostId) throws Exception;
}
