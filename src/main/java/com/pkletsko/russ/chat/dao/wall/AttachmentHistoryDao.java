package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.model.wall.AttachmentHistory;

public interface AttachmentHistoryDao {

    public Long addAttachment(final AttachmentHistory attachment) throws Exception;

    public boolean updateAttachment(final AttachmentHistory attachment) throws Exception;

    public AttachmentHistory getAttachmentById(final Long attachmentId) throws Exception;

}
