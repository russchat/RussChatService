package com.pkletsko.russ.chat.dao.school;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.school.School;
import com.pkletsko.russ.chat.model.school.SchoolClass;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.hibernate.Session;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import java.util.List;

public class SchoolDaoImpl extends AbstractDao implements SchoolDao {

    static Log log = LogFactory.getLog(SchoolDaoImpl.class.getName());

    @Override
    public boolean addSchool(final School school) throws Exception {
        getSession().save(school);

        return false;
    }

    @Override
    public boolean addSchoolClass(final SchoolClass schoolClass) throws Exception {
        getSession().save(schoolClass);

        return false;
    }

    @Override
    public School getSchoolById(final int schoolId) throws Exception {
        return (School) getSession().get(School.class, schoolId);
    }

    @Override
    public SchoolClass getSchoolClassById(final int schoolClassId) throws Exception {
        return (SchoolClass) getSession().get(SchoolClass.class, schoolClassId);
    }

    @Override
    public List<School> getSchoolList() throws Exception {
        return getSession().createCriteria(School.class).list();
    }

    @Override
    public List<School> search(final String searchString) {


        BooleanQuery bQuery = new BooleanQuery();
        Session session = getSession();

        FullTextSession fullTextSession = Search.getFullTextSession(session);

        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Analyzer analyzer = fullTextSession.getSearchFactory().getAnalyzer("schoolsearchtokenanalyzer");

        QueryParser parser = new QueryParser("schoolName", analyzer);

        String[] tokenized = null;

        try {
            Query query = parser.parse(searchString);
            String cleanedText = query.toString("schoolName");
            tokenized = cleanedText.split("\\s");


        } catch (org.apache.lucene.queryparser.classic.ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        QueryBuilder qBuilder = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(School.class).get();

        for (int i = 0; i < tokenized.length; i++) {
            if (i == (tokenized.length - 1)) {
                Query query = qBuilder.keyword().wildcard().onField("schoolName")
                        .matching(tokenized[i] + "*").createQuery();
                bQuery.add(query, BooleanClause.Occur.MUST);
            } else {
                Term exactTerm = new Term("schoolName", tokenized[i]);
                bQuery.add(new TermQuery(exactTerm), BooleanClause.Occur.MUST);
            }
        }

//        for (User exGoal : existingGoals) {
//            Term omittedTerm = new Term("id", String.valueOf(exGoal.getUserId()));
//            bQuery.add(new TermQuery(omittedTerm), BooleanClause.Occur.MUST_NOT);
//        }

        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bQuery, School.class);

        List<School> results = hibQuery.list();

        return results;
    }

    @Override
    public List<SchoolClass> searchSchoolClass(final String searchString) {


        BooleanQuery bQuery = new BooleanQuery();
        Session session = getSession();

        FullTextSession fullTextSession = Search.getFullTextSession(session);

        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Analyzer analyzer = fullTextSession.getSearchFactory().getAnalyzer("schoolclasssearchtokenanalyzer");

        QueryParser parser = new QueryParser("className", analyzer);

        String[] tokenized = null;

        try {
            Query query = parser.parse(searchString);
            String cleanedText = query.toString("className");
            tokenized = cleanedText.split("\\s");


        } catch (org.apache.lucene.queryparser.classic.ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        QueryBuilder qBuilder = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(SchoolClass.class).get();

        for (int i = 0; i < tokenized.length; i++) {
            if (i == (tokenized.length - 1)) {
                Query query = qBuilder.keyword().wildcard().onField("className")
                        .matching(tokenized[i] + "*").createQuery();
                bQuery.add(query, BooleanClause.Occur.MUST);
            } else {
                Term exactTerm = new Term("className", tokenized[i]);
                bQuery.add(new TermQuery(exactTerm), BooleanClause.Occur.MUST);
            }
        }

//        for (User exGoal : existingGoals) {
//            Term omittedTerm = new Term("id", String.valueOf(exGoal.getUserId()));
//            bQuery.add(new TermQuery(omittedTerm), BooleanClause.Occur.MUST_NOT);
//        }

        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bQuery, SchoolClass.class);

        List<SchoolClass> results = hibQuery.list();

        return results;
    }


}