package com.pkletsko.russ.chat.dao.conversation;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.conversation.Conversation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.Set;

public class ConversationDaoImpl extends AbstractDao implements ConversationDao {

    static Log log = LogFactory.getLog(ConversationDaoImpl.class.getName());

    private final int UP = 0;
    private final int DOWN = 1;

	@Override
	public Conversation addConversation(final Conversation conversation) throws Exception {
        if (conversation == null) {
            return null;
        }
        // create a new conversation
        Long  generatedId = (Long) getSession().save(conversation);
        log.debug("a new conversation : " + generatedId);

        return getConversationById(generatedId);
	}

	@Override
	public Conversation getConversationById(final Long conversationId) throws Exception {
        if (conversationId == null){
            return null;
        }

        return (Conversation) getSession().get(Conversation.class, conversationId);
	}

    @Override
    public Set<Conversation> getConversations(Long userId, Long lastLoadedConversationId, int itemsPerPage, Integer direction) throws Exception {

//                getSession().createCriteria(Conversation.class)
//                .addOrder(Order.desc("conversationId"))
//                .setMaxResults(5)
//                .add(Restrictions.lt("conversationId", 7L))
//                .createCriteria("users")
//                .add(Restrictions.like("userId", userId))
//                .list();

        Criteria cr = getSession().createCriteria(Conversation.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("conversationId"));
        } else {
            cr.addOrder(Order.desc("conversationId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (lastLoadedConversationId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("conversationId", lastLoadedConversationId));
            } else {
                cr.add(Restrictions.lt("conversationId", lastLoadedConversationId));
            }
        }

        cr.createCriteria("users").add(Restrictions.like("userId", userId));

        Set<Conversation> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }
}
