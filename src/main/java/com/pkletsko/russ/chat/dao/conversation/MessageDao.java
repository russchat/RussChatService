package com.pkletsko.russ.chat.dao.conversation;

import com.pkletsko.russ.chat.model.conversation.ConversationMessage;

import java.util.Set;

public interface MessageDao {

    Long addMessage(final ConversationMessage message) throws Exception;

    ConversationMessage getMessageById(final Long messageId) throws Exception;

    boolean updatedMessage(final ConversationMessage message) throws Exception;

    Set<ConversationMessage> getConversationMessages(Long conversationId, Long lastLoadedMsgId, int itemsPerPage, Integer direction);
}
