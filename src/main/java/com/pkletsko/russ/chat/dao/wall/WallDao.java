package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.model.wall.Wall;

public interface WallDao {

	public Wall addWall(final Wall wall) throws Exception;

    public boolean updateWall(final Wall wall) throws Exception;

    public Wall getWallById(final Long wallId) throws Exception;

}
