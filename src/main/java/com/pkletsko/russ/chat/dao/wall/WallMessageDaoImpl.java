package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.wall.CommentHistory;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.Set;

public class WallMessageDaoImpl extends AbstractDao implements WallMessageDao {

    static Log log = LogFactory.getLog(WallMessageDaoImpl.class.getName());

    private final int UP = 0;
    private final int DOWN = 1;

	@Override
	public WallMessage addWallMessage(final WallMessage wallMessage) throws Exception {
        if (wallMessage == null) {
             return null;
        }

        WallMessage newWallMessage = (WallMessage) getSession().merge(wallMessage);
        getSession().save(newWallMessage);
        log.debug("wall message id : " + newWallMessage.getMessageId() + " was added.");

        return newWallMessage;
	}

    @Override
    public boolean updateWallMessage(final WallMessage wallMessage) throws Exception {
        if (wallMessage == null) {
            return false;
        }

        getSession().update(wallMessage);
        log.debug("wall message id : " + wallMessage.getMessageId() + " was updated.");
        return true;
    }

	@Override
	public WallMessage getWallMessageById(final Long wallMessageId) throws Exception {
        if (wallMessageId == null) {
            return null;
        }

        return (WallMessage) getSession().get(WallMessage.class, wallMessageId);
	}

    @Override
    public Set<CommentHistory> getComments(Long wallPostId, Long fromCommentId, int itemsPerPage, int direction) throws Exception {

        Criteria cr = getSession().createCriteria(CommentHistory.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("commentId"));
        } else {
            cr.addOrder(Order.desc("commentId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (fromCommentId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("commentId", fromCommentId));
            } else {
                cr.add(Restrictions.lt("commentId", fromCommentId));
            }
        }
        cr.add(Restrictions.eq("wallMessage.messageId", wallPostId));

        Set<CommentHistory> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }

    @Override
    public Set<LikeHistory> getLikes(Long wallPostId, Long fromLikeId, int itemsPerPage, int direction) throws Exception {
        //getSession().createCriteria(CommentHistory.class).addOrder(Order.asc("commentId")).setMaxResults(5).add(Restrictions.gt("commentId", 0L)).add(Restrictions.eq("wallMessage.messageId", wallPostId)).list();

        Criteria cr = getSession().createCriteria(LikeHistory.class);

        if (direction == UP) {
            cr.addOrder(Order.asc("likeId"));
        } else {
            cr.addOrder(Order.desc("likeId"));
        }

        cr.setMaxResults(itemsPerPage);

        if (fromLikeId != 0L) {
            if (direction == UP) {
                cr.add(Restrictions.gt("likeId", fromLikeId));
            } else {
                cr.add(Restrictions.lt("likeId", fromLikeId));
            }
        }
        cr.add(Restrictions.eq("wallMessage.messageId", wallPostId));

        Set<LikeHistory> result = new HashSet<>();
        result.addAll(cr.list());
        return result;
    }
}
