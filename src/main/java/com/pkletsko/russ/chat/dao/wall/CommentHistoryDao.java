package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.model.wall.CommentHistory;

public interface CommentHistoryDao {

    public Long addCommentHistory(final CommentHistory commentHistory) throws Exception;

    public boolean updateCommentHistory(final CommentHistory commentHistory) throws Exception;

    public CommentHistory getCommentHistoryById(final Long commentHistoryId) throws Exception;

}
