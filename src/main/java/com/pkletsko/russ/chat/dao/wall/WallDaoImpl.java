package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.wall.Wall;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WallDaoImpl extends AbstractDao implements WallDao {

    static Log log = LogFactory.getLog(WallDaoImpl.class.getName());

	@Override
	public Wall addWall(final Wall wall) throws Exception {
        if (wall == null) {
            return null;
        }

        Long wallId = (Long) getSession().save(wall);
        log.debug("wall id : " + wallId + " was added.");

		return getWallById(wallId);
	}

    @Override
    public boolean updateWall(final Wall wall) throws Exception {
        if (wall == null) {
            return false;
        }

        getSession().update(wall);
        log.debug("wall id : " + wall.getWallId() + " was updated.");

        return true;
    }

    @Override
	public Wall getWallById(final Long wallId) throws Exception {
        if (wallId == null) {
            return null;
        }

        return (Wall) getSession().get(Wall.class, wallId);
	}

}
