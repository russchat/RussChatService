package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class LikeHistoryDaoImpl extends AbstractDao implements LikeHistoryDao {

    static Log log = LogFactory.getLog(LikeHistoryDaoImpl.class.getName());

	@Override
	public Long addLikeHistory(final LikeHistory likeHistory) throws Exception {
        if (likeHistory == null) {
             return null;
        }

        Long likeHistoryId = (Long) getSession().save(likeHistory);
        log.debug("like history id : " + likeHistoryId + " was added.");

        return likeHistoryId;
	}

    @Override
    public boolean updateLikeHistory(final LikeHistory likeHistory) throws Exception {
        if (likeHistory == null) {
            return false;
        }

        getSession().saveOrUpdate(likeHistory);
        log.debug("like history id : " + likeHistory.getLikeId() + " was updated.");

        return true;
    }

	@Override
	public LikeHistory getLikeHistoryById(final Long likeHistoryId) throws Exception {
        if (likeHistoryId == null) {
            return null;
        }

        return (LikeHistory) getSession().get(LikeHistory.class, likeHistoryId);
	}

    @Override
    public LikeHistory getLikeHistory(final Long userId, final Long wallPostId) throws Exception {
        if (userId == null || wallPostId == null) {
            return null;
        }

        //find requested user by loginId
        List<LikeHistory> list = getSession().createQuery("from LikeHistory where userId=? and wallMessageId=?").setParameter(0, userId).setParameter(1, wallPostId).list();

        // this userId is not existed in database
        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }

    /**
     *
     * @param likeHistoryId
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean deleteLikeHistory(final Long likeHistoryId) throws Exception {
        log.debug("delete like id: " + likeHistoryId);
        if (likeHistoryId == null) {
            return false;
        }

        LikeHistory likeHistory = getLikeHistoryById(likeHistoryId);
        if (likeHistory != null) {
            getSession().delete(likeHistory);
        }
        return false;
    }
}
