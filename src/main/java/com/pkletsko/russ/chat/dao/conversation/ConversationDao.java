package com.pkletsko.russ.chat.dao.conversation;

import com.pkletsko.russ.chat.model.conversation.Conversation;

import java.util.Set;

public interface ConversationDao {

	Conversation addConversation(final Conversation conversation) throws Exception;

    Conversation getConversationById(final Long conversationId) throws Exception;

    Set<Conversation> getConversations(Long userId, Long lastLoadedConversationId, int itemsPerPage, Integer direction)  throws Exception;
}
