package com.pkletsko.russ.chat.dao.wall;

import com.pkletsko.russ.chat.dao.common.AbstractDao;
import com.pkletsko.russ.chat.model.wall.AttachmentHistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AttachmentHistoryDaoImpl extends AbstractDao implements AttachmentHistoryDao {

    static Log log = LogFactory.getLog(AttachmentHistoryDaoImpl.class.getName());

	@Override
	public Long addAttachment(final AttachmentHistory attachment) throws Exception {
        if (attachment == null) {
             return null;
        }

        Long attachmentId = (Long) getSession().save(attachment);
        log.debug("Attachment id : " + attachmentId + " was added.");

        return attachmentId;
	}

    @Override
    public boolean updateAttachment(final AttachmentHistory attachment) throws Exception {
        if (attachment == null) {
            return false;
        }

        getSession().saveOrUpdate(attachment);
        log.debug("Attachment id : " + attachment.getAttachmentId() + " was updated.");

        return true;
    }

	@Override
	public AttachmentHistory getAttachmentById(final Long attachmentId) throws Exception {
        if (attachmentId == null) {
            return null;
        }

        return (AttachmentHistory) getSession().get(AttachmentHistory.class, attachmentId);
	}
}
