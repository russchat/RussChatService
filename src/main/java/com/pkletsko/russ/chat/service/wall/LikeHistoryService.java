package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Likes;

import java.util.Set;

public interface LikeHistoryService {

    boolean addReceived(final Long likeHistoryId, final Long userId) throws Exception;

    boolean removeUnReceived(final Long likeHistoryId, final Long userId) throws Exception;

    String getUnReceived(final Long userId) throws Exception;

    BroadcastMessage prepareBroadcastMessage(final String clientMessage, final User user, MessageObjectType messageType) throws Exception;

    Likes getLikes(final Long wallPostId, final Long lastLoadedUserLikedId, int itemsPerPage, final Long userId, final Integer direction) throws Exception;
}
