package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.CommentHistory;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPosts;

import java.util.List;
import java.util.Set;

public interface WallMessageService {

    boolean addReceived(final WallMessage wallMessage, final List<Long> userIds) throws Exception;
    boolean addReceived(final Long wallMessageid, final Long userIds) throws Exception;

    boolean removeUnReceived(final Long wallPostId, final Long userId) throws Exception;

    String getUnReceived(int limit, final Long userId) throws Exception;

    BroadcastMessage prepareBroadcastMessage(final String clientMessage, final User user, MessageObjectType messageType) throws Exception;

    WallPosts getReceivedUserPosts(final Long loggedUserId, final Long lastLoadedWallPostId, int itemsPerPage, final Integer direction) throws Exception;


    WallPosts getUserPosts(final Long userId, final Long loggedUserId, final Long lastLoadedWallPostId, int itemsPerPage, final Integer direction) throws Exception;
}
