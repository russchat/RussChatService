package com.pkletsko.russ.chat.service.conversation;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessages;

public interface ConversationMessageService {

    boolean addReceived(final Long messageId, final Long userId) throws Exception;

    String getUnReceived(final Long userId) throws Exception;

    ChatMessages getConversationMessages(final Long conversationId, final Long lastLoadedMsgId, int itemsPerPage, final Long userId, final Integer direction);

    BroadcastMessage prepareBroadcastMessage(final User user, final String messageBody) throws Exception;
}
