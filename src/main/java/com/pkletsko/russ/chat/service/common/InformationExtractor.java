package com.pkletsko.russ.chat.service.common;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.user.UserComparator;
import com.pkletsko.russ.chat.model.wall.Wall;

import java.util.Set;
import java.util.TreeSet;

public class InformationExtractor {
    public static Long getMyWallId(final User user) {
        if (user == null || user.getMyWalls().isEmpty()) {
            return null;
        }

        return user.getMyWalls().iterator().next().getWallId();
    }

    public static Wall getMyWall(final User user) {
        if (user == null || user.getMyWalls().isEmpty()) {
            return null;
        }

        return user.getMyWalls().iterator().next();
    }

    public static Set<User> getMyFollowers(final User user) {
        if (user == null || user.getMyWalls().isEmpty()) {
            return null;
        }

        // set of users should be sorted by id
        Set<User> users = new TreeSet<>(new UserComparator());
        users.addAll(user.getMyWalls().iterator().next().getFollowingUsers());

        return users;
    }
}
