package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.dao.user.UserDao;
import com.pkletsko.russ.chat.dao.wall.WallDao;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.Wall;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class WallServiceImpl implements WallService {

    static Log log = LogFactory.getLog(WallServiceImpl.class.getName());

    @Autowired
    WallDao wallDao;

    @Autowired
    UserDao userDao;

    @Override
    public Wall addWall(User user) throws Exception {
        Set<User> users = new HashSet<>();
        users.add(user);

        Wall wall = new Wall();
        wall.setOwnerOfWall(user);
        wall.setFollowingUsers(users);
        return wallDao.addWall(wall);
    }

    @Override
    public Wall getWallById(final Long wallId) throws Exception {
        return wallDao.getWallById(wallId);
    }

    @Override
    public boolean addWallFollowers(final Long userId, final List<Long> wallIds) throws Exception {
        for (Long wallId: wallIds) {
            addWallFollower(userId, wallId);
        }
        return true;
    }

    @Override
    public boolean removeWallFollowers(final Long userId, final List<Long> wallIds) throws Exception {
        for (Long wallId: wallIds) {
            removeWallFollower(userId, wallId);
        }
        return true;
    }

    @Override
    public boolean addWallFollower(final Long userId, final Long wallId) throws Exception {
        User user = userDao.getUserById(userId);
        Wall currentWall = getWallById(wallId);

        if (user == null || currentWall == null) {
            return false;
        }

        currentWall.getFollowingUsers().add(user);


        return wallDao.updateWall(currentWall);
    }

    @Override
    public boolean removeWallFollower(final Long userId, final Long wallId) throws Exception {
        User user = userDao.getUserById(userId);
        Wall currentWall = getWallById(wallId);

        if (user == null || currentWall == null) {
            return false;
        }

        currentWall.getFollowingUsers().remove(user);


        return wallDao.updateWall(currentWall);
    }
}
