package com.pkletsko.russ.chat.service.school;

import com.pkletsko.russ.chat.model.school.School;
import com.pkletsko.russ.chat.model.school.SchoolClass;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;

import java.util.List;

public interface SchoolService {


	SchoolInfo getSchoolInfoById(final int schoolId) throws Exception;

	SchoolClassInfo getSchoolClassInfoById(final int schoolClassId) throws Exception;

    SchoolClass getSchoolClassById(final int schoolClassId) throws Exception;

	List<SchoolInfo> searchSchool(final String searchQuery) throws Exception;

	List<SchoolClassInfo> searchSchoolClass(final int schoolId, final String searchQuery) throws Exception;

	void addSchool(final String clientMessage) throws Exception;

    void addSchoolClass(final String clientMessage) throws Exception;
}
