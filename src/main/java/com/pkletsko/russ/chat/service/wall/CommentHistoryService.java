package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.CommentHistory;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comments;

import java.util.Set;

public interface CommentHistoryService {

    boolean addReceived(final Long commentHistoryId, final Long userId) throws Exception;

    boolean removeUnReceived(final Long commentHistoryId, final Long userId) throws Exception;

    String getUnReceived(final Long userId) throws Exception;

    BroadcastMessage prepareBroadcastMessage(final String message, final User user) throws Exception;

    Comments getComments(final Long wallPostId, final Long lastLoadedUserLikedId, int itemsPerPage, final Long userId, final Integer direction) throws Exception;
}
