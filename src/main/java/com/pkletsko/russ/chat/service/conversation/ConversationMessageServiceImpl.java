package com.pkletsko.russ.chat.service.conversation;

import com.pkletsko.russ.chat.dao.conversation.ConversationDao;
import com.pkletsko.russ.chat.dao.conversation.MessageDao;
import com.pkletsko.russ.chat.model.conversation.Conversation;
import com.pkletsko.russ.chat.model.conversation.ConversationMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.*;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.pkletsko.russ.chat.service.common.Converter.convertToChatMessage;
import static com.pkletsko.russ.chat.service.common.Converter.convertToChatMessages;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LIST_OF_CONVERSATION_MESSAGES;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UNRECEIVED_MESSAGES;

@Service
@Transactional
public class ConversationMessageServiceImpl implements ConversationMessageService {

    @Autowired
    MessageDao messageDao;

    @Autowired
    ConversationDao conversationDao;

    @Autowired
    UserService userService;

    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    ConversationService conversationService;

    @Autowired
    Global global;

    static Log log = LogFactory.getLog(ConversationMessageServiceImpl.class.getName());

    @Override
    public boolean addReceived(final Long messageId, final Long userId) throws Exception {
        ConversationMessage message = getMessageById(messageId);

        if (message == null) {
            return false;
        }

        User user = userService.getUserById(userId);

        message.addUsersReceivedTheMsg(user);
        return messageDao.updatedMessage(message);
    }

    @Override
    public String getUnReceived(final Long userId) throws Exception {
        UnReceivedChatMessages unReceivedChatMessages = getUnReceivedChatMessages(userId);
        addReceived(unReceivedChatMessages.getChatMessages(), userId);
        return jsonUtils.getGSONbyResponseObject(unReceivedChatMessages);
    }

    @Override
    public ChatMessages getConversationMessages(Long conversationId, Long lastLoadedMsgId, int itemsPerPage, Long userId, Integer direction) {

        log.debug("getting ChatMessages for conversation id: " + conversationId);
        ChatMessages response = new ChatMessages();
        response.setObjectType(MOT_LIST_OF_CONVERSATION_MESSAGES);

        if (lastLoadedMsgId == null) {
            return response;
        }

        final Set<ChatMessage> result = new TreeSet<>(new ChatMessageComp());
        final Set<UserProfile> userProfileSet = new HashSet<>();

        final Set<ConversationMessage> conversationMessages = messageDao.getConversationMessages(conversationId, lastLoadedMsgId, itemsPerPage, direction);

        // create an iterator
        final Iterator iterator = conversationMessages.iterator();

        // check values
        while (iterator.hasNext() && itemsPerPage != 0) {
            //TODO think about to sent minimal user details
            final ConversationMessage conversationMessage = (ConversationMessage) iterator.next();
            result.add(convertToChatMessage(conversationMessage));
            //userProfileSet.add(getUserProfile(commentHistory.getOwnerOfComment().getUserId(), userId));
        }

        response.setChatMessages(result);
        response.setUserProfiles(userProfileSet);

        return response;
    }

    @Override
    public BroadcastMessage prepareBroadcastMessage(User user, String messageBody) throws Exception {
        ChatMessage chatMessage = jsonUtils.getChatMessage(messageBody);
        chatMessage = saveChatMessage(user, chatMessage);
        final String json = jsonUtils.getGSONbyResponseObject(chatMessage);

        final Set<UserProfile> chatUsers = conversationService.getConversationUIById(chatMessage.getConversationId()).getUsers();
        Set<Long> userIds = new HashSet<>();
        for (final UserProfile chatUser : chatUsers) {
            userIds.add(chatUser.getUserId());
            //TODO Unreceived messages
        }

        return new BroadcastMessage(userIds, json, chatMessage.getMessageId());
    }

    private ChatMessage saveChatMessage(final User ownerOfChatMessage, final ChatMessage chatMessage)  throws Exception{

        ConversationMessage newMessage = new ConversationMessage();
        newMessage.setMessageText(chatMessage.getMessageText());
        newMessage.setOwnerOfMessage(ownerOfChatMessage);

        chatMessage.setMessageId(addMessage(newMessage, chatMessage.getConversationId()));

        return chatMessage;
    }

    private Long addMessage(final ConversationMessage message, final Long conversationId) throws Exception {
        Conversation conversation = conversationDao.getConversationById(conversationId);

        if (conversation == null || message == null) {
            return null;
        }

        message.setConversation(conversation);
        return messageDao.addMessage(message);
    }

    private ConversationMessage getMessageById(final Long messageId) throws Exception {
        if (messageId == null) {
            return null;
        }
        return messageDao.getMessageById(messageId);
    }

    private boolean addReceived(final Set<ChatMessage> chatMessages, final Long userId) throws Exception {
        //put unread messages to status read
        for (ChatMessage chatMessage : chatMessages) {
            addReceived(chatMessage.getMessageId(), userId);
        }

        return true;
    }

    private UnReceivedChatMessages getUnReceivedChatMessages(final Long userId) throws Exception {
        log.debug("getting getUnReceived for user id: " + userId);
        if (userId == null) {
            return null;
        }

        UnReceivedChatMessages unReceivedChatMessages = new UnReceivedChatMessages();
        Set<ChatMessage> allChatMessages = new HashSet<>();
        Set<Chat> allConversationUIs = new HashSet<>();

        User user = userService.getUserById(userId);

        if (user != null) {
            for (Conversation conversation : user.getConversations()) {
                Set<ConversationMessage> allConversationMessages;
                Set<ConversationMessage> messagesReceivedByCategory = new HashSet<>();
                for (ConversationMessage message : user.getReceivedMessages()) {
                    if (message.getConversation().getConversationId() == conversation.getConversationId()) {
                        messagesReceivedByCategory.add(message);
                    }
                }

                allConversationMessages = conversation.getMessages();
                allConversationMessages.removeAll(messagesReceivedByCategory);

                if (allConversationMessages.size() > 0) {
                    Set<ChatMessage> chatMessages = convertToChatMessages(allConversationMessages);

                    Chat conversationUI = new Chat();
                    conversationUI.setConversationId(conversation.getConversationId());
                    conversationUI.setConversationName(conversation.getConversationName());
                    conversationUI.setNewChatMessagesNumber(chatMessages.size());

                    allChatMessages.addAll(chatMessages);
                    allConversationUIs.add(conversationUI);
                }
            }

            unReceivedChatMessages.setChatMessages(allChatMessages);
            unReceivedChatMessages.setChats(allConversationUIs);
            unReceivedChatMessages.setObjectType(MOT_UNRECEIVED_MESSAGES);
        }
        return unReceivedChatMessages;
    }

}
