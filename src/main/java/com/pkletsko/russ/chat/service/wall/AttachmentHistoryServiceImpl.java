package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.dao.wall.AttachmentHistoryDao;
import com.pkletsko.russ.chat.dao.wall.WallMessageDao;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.AttachmentHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Attachment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AttachmentHistoryServiceImpl implements AttachmentHistoryService {

    static Log log = LogFactory.getLog(AttachmentHistoryServiceImpl.class.getName());

    @Autowired
    UserService userService;

    @Autowired
    WallMessageDao wallMessageDao;

    @Autowired
    AttachmentHistoryDao attachmentHistoryDao;

    @Override
    public Attachment saveAttachmentHistory(final Long ownerOfAttachmentHistoryId, final Attachment attachment)  throws Exception {

        User ownerOfAttachmentHistory = userService.getUserById(ownerOfAttachmentHistoryId);

        AttachmentHistory newAttachmentHistory = new AttachmentHistory();
        newAttachmentHistory.setOwnerOfAttachment(ownerOfAttachmentHistory);
        newAttachmentHistory.setAttachmentName(attachment.getAttachmentName());

        attachment.setAttachmentId(addAttachmentHistory(newAttachmentHistory, attachment.getWallMessageId()));
        attachment.setFromUserId(ownerOfAttachmentHistory.getUserId());

        return attachment;
    }


    private Long addAttachmentHistory(final AttachmentHistory attachmentHistory, final Long wallMessageId) throws Exception {
        WallMessage currentWallMessage = wallMessageDao.getWallMessageById(wallMessageId);
        if (currentWallMessage == null) {
            return null;
        }

        attachmentHistory.setWallMessage(currentWallMessage);

        return attachmentHistoryDao.addAttachment(attachmentHistory);
    }
}
