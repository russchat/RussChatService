package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.websocket.message.json.model.wall.Attachment;

public interface AttachmentHistoryService {

    Attachment saveAttachmentHistory(final Long userId, final Attachment attachment)  throws Exception;

}
