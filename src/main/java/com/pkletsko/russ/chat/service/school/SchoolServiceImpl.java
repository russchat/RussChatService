package com.pkletsko.russ.chat.service.school;

import com.pkletsko.russ.chat.dao.school.SchoolDao;
import com.pkletsko.russ.chat.model.school.School;
import com.pkletsko.russ.chat.model.school.SchoolClass;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.pkletsko.russ.chat.service.common.Converter.*;

@Service
@Transactional
public class SchoolServiceImpl implements SchoolService {

    static Log log = LogFactory.getLog(SchoolServiceImpl.class.getName());

    @Autowired
    SchoolDao schoolDao;

    @Autowired
    JSONUtils jsonUtils;

    @Override
    public SchoolInfo getSchoolInfoById(final int schoolId) throws Exception{
        return convertToSchoolInfo(schoolDao.getSchoolById(schoolId));
    }

    @Override
    public SchoolClassInfo getSchoolClassInfoById(final int schoolClassId) throws Exception{
        return convertToSchoolClassInfo(schoolDao.getSchoolClassById(schoolClassId));
    }

    @Override
    public SchoolClass getSchoolClassById(final int schoolClassId) throws Exception {
        return schoolDao.getSchoolClassById(schoolClassId);
    }

    @Override
    public List<SchoolInfo> searchSchool(final String searchQuery) throws Exception {
        return convertToSchoolInfos(schoolDao.search(searchQuery));
    }

    @Override
    public List<SchoolClassInfo> searchSchoolClass(final int schoolId, final String searchQuery) throws Exception {
        return convertToSchoolClassInfos(schoolId, schoolDao.searchSchoolClass(searchQuery));
    }

    @Override
    public void addSchool(String clientMessage) throws Exception {
        SchoolInfo schoolInfo = jsonUtils.getSchoolInfoRequest(clientMessage);

        School school = new School();
        school.setSchoolName(schoolInfo.getSchoolName());
        school.setCountry(schoolInfo.getCountry());
        school.setCity(schoolInfo.getCity());
        school.setZip(schoolInfo.getZip());
        school.setAddress(schoolInfo.getAddress());
        school.setTelephone(schoolInfo.getTelephone());

        addSchool(school);
    }

    @Override
    public void addSchoolClass(String clientMessage) throws Exception {
        SchoolClassInfo schoolClassInfo = jsonUtils.getSchoolClassInfoRequest(clientMessage);

        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setClassName(schoolClassInfo.getClassName());
        schoolClass.setSchool(getSchoolById(schoolClassInfo.getSchool().getSchoolId()));

        addSchoolClass(schoolClass);
    }

    private boolean addSchool(final School school) throws Exception {
        return schoolDao.addSchool(school);
    }

    private boolean addSchoolClass(final SchoolClass schoolClass) throws Exception {
        return schoolDao.addSchoolClass(schoolClass);
    }

    private School getSchoolById(final int schoolId) throws Exception {
        return schoolDao.getSchoolById(schoolId);
    }
}
