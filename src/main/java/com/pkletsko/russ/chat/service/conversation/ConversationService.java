package com.pkletsko.russ.chat.service.conversation;

import com.pkletsko.russ.chat.model.conversation.Conversation;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Conversations;

import java.util.List;

public interface ConversationService {


    Chat getConversationUIById(final Long conversationId) throws Exception;

    Conversations getConversations(final Long userId, final Long lastLoadedConversationId, final int numberOfItemsPerPage, final Integer direction)  throws Exception;

    String getRequestToReNewConversation(final String clientMessage) throws Exception;

    BroadcastMessage prepareBroadcastMessage(String clientMessage, User user) throws Exception;

    String getSearchResult(String clientMessage) throws Exception;
}
