package com.pkletsko.russ.chat.service.common;

import com.pkletsko.russ.chat.model.conversation.Conversation;
import com.pkletsko.russ.chat.model.conversation.ConversationMessage;
import com.pkletsko.russ.chat.model.school.School;
import com.pkletsko.russ.chat.model.school.SchoolClass;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.user.UserComparator;
import com.pkletsko.russ.chat.model.wall.*;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;

import java.util.*;

import static com.pkletsko.russ.chat.service.common.InformationExtractor.*;
import static com.pkletsko.russ.chat.service.common.InformationExtractor.getMyFollowers;
import static com.pkletsko.russ.chat.service.common.InformationExtractor.getMyWallId;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.*;

/**
 * Created by pkletsko on 21.12.2015.
 */
public class Converter {

    public static Like convertToLike(final LikeHistory likeHistory) throws Exception {
        Like like = new Like();
        like.setLikeId(likeHistory.getLikeId());
        like.setDisabled(likeHistory.isDisabled());
        like.setFromUserId(likeHistory.getOwnerOfLike().getUserId());
        like.setWallMessageId(likeHistory.getWallMessage().getMessageId());

        if (likeHistory.isDisabled()) {
            like.setObjectType(MOT_DISLIKE);
        } else {
            like.setObjectType(MOT_LIKE);
        }

        return like;
    }

    public static ChatMessage convertToChatMessage(final ConversationMessage conversationMessage) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setConversationId(conversationMessage.getConversation().getConversationId());
        chatMessage.setFromUserId(conversationMessage.getOwnerOfMessage().getUserId());
        chatMessage.setFromUserName(conversationMessage.getOwnerOfMessage().getFullName());
        chatMessage.setMessageId(conversationMessage.getMessageId());
        chatMessage.setMessageText(conversationMessage.getMessageText());
        chatMessage.setObjectType(MOT_NEW_MESSAGE);
        return chatMessage;
    }

    public static List<SchoolClassInfo> convertToSchoolClassInfos(final int schoolId, List<SchoolClass> schoolClasses) {
        List<SchoolClassInfo> schoolClassInfos = new ArrayList<>();
        for(SchoolClass schoolClass: schoolClasses) {
            if (schoolClass.getSchool().getSchoolId() == schoolId) {
                schoolClassInfos.add(convertToSchoolClassInfo(schoolClass));
            }
        }

        return schoolClassInfos;
    }

    public static List<SchoolInfo> convertToSchoolInfos(List<School> schools) {
        List<SchoolInfo> schoolInfos = new ArrayList<>();
        for(School school:schools) {
            schoolInfos.add(convertToSchoolInfo(school));
        }

        return schoolInfos;
    }

    public static SchoolInfo convertToSchoolInfo(School school) {
        SchoolInfo searchResult = new SchoolInfo();
        searchResult.setSchoolName(school.getSchoolName());
        searchResult.setSchoolId(school.getSchoolId());
        searchResult.setAddress(school.getAddress());
        searchResult.setCity(school.getCity());
        searchResult.setCountry(school.getCountry());
        searchResult.setTelephone(school.getTelephone());
        searchResult.setZip(school.getZip());
        searchResult.setObjectType(MessageObjectType.MOT_CREATE_NEW_SCHOOL);

        return searchResult;
    }

    public static SchoolClassInfo convertToSchoolClassInfo(SchoolClass schoolClass) {
        SchoolClassInfo searchResult = new SchoolClassInfo();
        searchResult.setClassName(schoolClass.getClassName());
        searchResult.setSchoolClassId(schoolClass.getSchoolClassId());
        searchResult.setSchool(convertToSchoolInfo(schoolClass.getSchool()));
        searchResult.setObjectType(MessageObjectType.MOT_CREATE_NEW_SCHOOL_CLASS);
        return searchResult;
    }

    public static Set<ChatMessage> convertToChatMessages(final Set<ConversationMessage> messages) {
        Set<ChatMessage> chatMessages = new HashSet<>();

        for (final ConversationMessage message : messages) {
            chatMessages.add(convertToChatMessage(message));
        }

        return chatMessages;
    }

    public static WallPost convertToWallPost(final WallMessage wallMessage) {
        if (wallMessage == null) {
            return null;
        }

        //unreceived wall message wrapped to WallMessageUI
        WallPost wallPost = new WallPost();
        wallPost.setObjectType(MOT_NEW_WALL_MESSAGE);
        wallPost.setWallId(wallMessage.getWall().getWallId());
        wallPost.setMessageText(wallMessage.getMessageText());
        wallPost.setFromUserId(wallMessage.getOwnerOfWallMessage().getUserId());
        wallPost.setWallMessageId(wallMessage.getMessageId());
        wallPost.setCreated(wallMessage.getCreated());

        if (wallMessage.getComments().size() > 0) {
            wallPost.setCommentNumber(wallMessage.getComments().size());
        }

        if (wallMessage.getLikes().size() > 0) {
            wallPost.setLikeNumber(wallMessage.getLikes().size());
            wallPost.setLiked(true);
        }

        if (wallMessage.getAttachments().size() > 0) {
            wallPost.setImageAttached(true);
            Set<String> attachments = new HashSet<>();

            for (AttachmentHistory attachment : wallMessage.getAttachments()) {
                attachments.add(attachment.getAttachmentName());
            }

            wallPost.setAttachments(attachments);
        }

        return wallPost;
    }

    public static Set<WallPost> convertToWallPosts(final Set<WallMessage> wallMessages) {
        Set<WallPost> wallPosts = new TreeSet<>(new WallPostComp());

        for (WallMessage wallMessage : wallMessages) {
            wallPosts.add(Converter.convertToWallPost(wallMessage));
        }

        return wallPosts;
    }

    public static Set<Like> convertToLikeSet(final Set<LikeHistory> likeHistories) throws Exception {
        Set<Like> likes = new HashSet<>();

        for (LikeHistory likeHistory : likeHistories) {
            likes.add(convertToLike(likeHistory));
        }

        return likes;
    }

    public static Set<Comment> convertToCommentSet(final Set<CommentHistory> commentHistories) {
        Set<Comment> comments = new HashSet<>();

        for (CommentHistory commentHistory : commentHistories) {
            comments.add(convertToComment(commentHistory));
        }

        return comments;
    }

    public static Comment convertToComment(final CommentHistory commentHistory) {
        Comment comment = new Comment();
        comment.setCommentId(commentHistory.getCommentId());
        comment.setWallMessageId(commentHistory.getWallMessage().getMessageId());
        comment.setFromUserId(commentHistory.getOwnerOfComment().getUserId());
        comment.setCommentText(commentHistory.getCommentText());
        comment.setObjectType(MOT_COMMENT);

        return comment;
    }

    public static UserProfile convertToUserProfile(final User user, final Boolean followed) {
        if (user == null) {
            return null;
        }

        UserProfile profile = new UserProfile();

        //basic information about user
        profile.setFollowed(followed);
        profile.setUserId(user.getUserId());
        profile.setProfileIcon(user.getProfileIcon());
        profile.setMyWallId(getMyWallId(user));
        profile.setFirstName(user.getFirstName());
        profile.setLastName(user.getLastName());
        profile.setFullName(user.getFullName());
        profile.setNickName(user.getNickName());
        profile.setCountry(user.getCountry());
        profile.setCity(user.getCity());
        profile.setColour(user.getColour());
        profile.setVersion(user.getVersion());

        if (user.getSchoolClass() != null) {
            profile.setSchool(user.getSchoolClass().getSchool().getSchoolId());
            profile.setSchoolClass(user.getSchoolClass().getSchoolClassId());
        }

        // get number of followers (who following current user)
        Set<User> followers = getMyFollowers(user);

        // remove himself (because current user always following himself to receive their owen messages)
        if (followers != null) {
            followers.remove(user);
            profile.setFollowers(followers.size());
        }

        // get number of following users (current user following these users)
        Set<Wall> following = user.getWalls();

        // remove himself (because current user always following himself to receive their owen messages)
        following.remove(getMyWall(user));
        profile.setFollowingWalls(following.size());

        // get total number of posts of current user
        profile.setMyPosts(user.getMyWallMessages().size());

        List<Long> followedWalls = new ArrayList<>();

        for (Wall wall : user.getWalls()) {
            if (wall.getOwnerOfWall().getUserId() != user.getUserId()) {
                followedWalls.add(wall.getWallId());
            }
        }

        profile.setFollowedWalls(followedWalls);

        return profile;
    }

    public static Set<UserProfile> convertToUserProfileSet(final List<User> users) {
        Set<UserProfile> userProfileSet = new HashSet<>();
        for (User user : users) {
            userProfileSet.add(convertToUserProfile(user, null));
        }
        return userProfileSet;
    }

    public static Set<UserProfile> convertToUserProfileSet(final Set<User> users) {
        Set<UserProfile> userProfileSet = new HashSet<>();
        for (User user : users) {
            userProfileSet.add(convertToUserProfile(user, null));
        }
        return userProfileSet;
    }


    public static Chat convertToConversationUI(final Conversation existedConversation) {
        if (existedConversation == null) {
            return null;
        }

        Chat conversationUI = new Chat();
        conversationUI.setConversationId(existedConversation.getConversationId());
        conversationUI.setConversationName(existedConversation.getConversationName());
        conversationUI.setUsers(convertToUserProfileSet(existedConversation.getUsers()));

        return conversationUI;
    }

}
