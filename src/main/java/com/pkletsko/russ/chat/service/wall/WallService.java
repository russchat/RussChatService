package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.Wall;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallDescription;

import java.util.List;

public interface WallService {

	Wall addWall(User user) throws Exception;

    Wall getWallById(final Long wallId) throws Exception;

    boolean addWallFollower(final Long newUserId, final Long wallId) throws Exception;

    boolean addWallFollowers(final Long userId, final List<Long> wallIds) throws Exception;

    boolean removeWallFollower(final Long userId, final Long wallId) throws Exception;

    boolean removeWallFollowers(final Long userId, final List<Long> wallIds) throws Exception;

}
