package com.pkletsko.russ.chat.service.conversation;

import com.pkletsko.russ.chat.dao.conversation.ConversationDao;
import com.pkletsko.russ.chat.dao.user.UserDao;
import com.pkletsko.russ.chat.model.conversation.Conversation;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.*;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchResult;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.pkletsko.russ.chat.service.common.Converter.convertToConversationUI;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CHAT_USER_SEARCH;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CREATE_NEW_CONVERSATIONS;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LIST_OF_CONVERSATIONS;

@Service
@Transactional
public class ConversationServiceImpl implements ConversationService {

    static Log log = LogFactory.getLog(ConversationServiceImpl.class.getName());

    @Autowired
    ConversationDao conversationDao;

    @Autowired
    UserDao userDao;

    @Autowired
    private UserService userService;

    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    Global global;

    @Override
    public Chat getConversationUIById(final Long conversationId) throws Exception {
        return convertToConversationUI(getConversationById(conversationId));
    }

    @Override
    public Conversations getConversations(final Long userId, final Long lastLoadedConversationId, final int itemsPerPage, final Integer direction)  throws Exception {

        log.debug("getting comments for wall post id: " + userId);
        Conversations response = new Conversations();
        response.setObjectType(MOT_LIST_OF_CONVERSATIONS);

        if (lastLoadedConversationId == null) {
            return response;
        }

        final Set<Chat> result = new TreeSet<>(new ConversationComp());
        final Set<UserProfile> userProfileSet = new HashSet<>();

        final Set<Conversation> conversations = conversationDao.getConversations(userId, lastLoadedConversationId, itemsPerPage, direction);

        // create an iterator
        final Iterator iterator = conversations.iterator();

        // check values
        while (iterator.hasNext() && itemsPerPage != 0) {
            //TODO think about to sent minimal user details
            final Conversation conversation = (Conversation) iterator.next();
            result.add(convertToConversationUI(conversation));
            //userProfileSet.add(getUserProfile(commentHistory.getOwnerOfComment().getUserId(), userId));
        }

        response.setConversations(result);
        response.setUserProfiles(userProfileSet);

        return response;
    }

    @Override
    public String getRequestToReNewConversation(String clientMessage) throws Exception {
        ReNewChat reNewConversationRequest = jsonUtils.getReNewConversationRequest(clientMessage);
        NewChat newConversationResponse = new NewChat();
        newConversationResponse.setObjectType(MOT_CREATE_NEW_CONVERSATIONS);
        newConversationResponse.setChat(getConversationUIById(reNewConversationRequest.getConversationId()));
        return jsonUtils.getGSONbyResponseObject(newConversationResponse);
    }

    @Override
    public BroadcastMessage prepareBroadcastMessage(String clientMessage, User currentUser) throws Exception {
        CreateNewChat createNewConversationRequest = jsonUtils.getCreateNewConversationRequest(clientMessage);

        Chat newConversation = createNewConversation(currentUser.getUserId(), createNewConversationRequest.getUserList(), createNewConversationRequest.getConversationName());

        NewChat newConversationResponse = new NewChat();
        newConversationResponse.setObjectType(MOT_CREATE_NEW_CONVERSATIONS);
        newConversationResponse.setChat(newConversation);
        newConversationResponse.setUserProfile(userService.getUserProfile(currentUser.getUserId()));

        String json = jsonUtils.getGSONbyResponseObject(newConversationResponse);

        Set<Long> userIds = new HashSet<>();

        //Iterate Users and compare with active session users
        for (final UserProfile chatUser : newConversation.getUsers()) {
            userIds.add(chatUser.getUserId());
           //TODO user is not able to receive the message
        }

        return new BroadcastMessage(userIds, json, newConversation.getConversationId());
    }

    @Override
    public String getSearchResult(String clientMessage) throws Exception {
        SearchRequest searchRequest = jsonUtils.getSearchRequest(clientMessage);
        SearchResult searchResult = userService.searchUser(searchRequest.getSearchQuery());
        searchResult.setObjectType(MOT_CHAT_USER_SEARCH);
        return jsonUtils.getGSONbyResponseObject(searchResult);
    }

    private Chat createNewConversation(final Long currentUserId, final List<Long> userIds, final String conversationName) throws Exception {
        Set<User> users = new HashSet<>();

        users.add(userDao.getUserById(currentUserId));

        for (Long userId: userIds){
            users.add(userDao.getUserById(userId));
        }

        Conversation conversation = new Conversation();
        conversation.setConversationName(conversationName);
        conversation.setUsers(users);

        return  addConversation(currentUserId, conversation);
    }

    private Chat addConversation(final Long userId, final Conversation conversation) throws Exception {
        User user = userDao.getUserById(userId);
        if (user == null) {
            return null;
        }

        Conversation existedConversation = isConversationExist(user, conversation);

        if (existedConversation == null) {
            // create a new conversation
            existedConversation = conversationDao.addConversation(conversation);
        }

        return convertToConversationUI(existedConversation);
    }

    private Conversation getConversationById(final Long conversationId) throws Exception {
        return conversationDao.getConversationById(conversationId);
    }

    private Conversation isConversationExist(final User user, final Conversation conversation) {
        int newConversationUserNumber = conversation.getUsers().size();

        //check if the conversation already exist (the conversation exist if the same users are already belongs to the conversation)

        //1- check all conversations the user belongs to
        for (final Conversation userConversation : user.getConversations()) {

            //get all users of the conversation
            Set<User> usersOfConversation = userConversation.getUsers();


            //2 - check the number of conversation (if it is equal , it could be existed conversation )
            if (usersOfConversation.size() == newConversationUserNumber) {
                int commonUserNumber = 0;

                //3 - compare users from new conversation with existed
                //iterate users from current conversation
                for (User conversationUser: usersOfConversation){
                    //iterate users from the conversation we are trying to create
                    for (User newUser: conversation.getUsers()){
                        //compare id(s)
                        if (newUser.getUserId() == conversationUser.getUserId()) {
                            //common users in this conversation and new conversation
                            commonUserNumber++;
                        }
                    }
                }

                // 4 - comparing if number of users from new conversation equal to number common users between new and current conversations
                if (newConversationUserNumber == commonUserNumber){
                    //we found existed conversation. So we should not create it again. We should return existed conversation.
                    return userConversation;
                }
            }
        }

        return null;
    }


}
