package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.dao.user.UserDao;
import com.pkletsko.russ.chat.dao.wall.WallDao;
import com.pkletsko.russ.chat.dao.wall.WallMessageDao;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.Wall;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.pkletsko.russ.chat.service.common.Converter.convertToWallPosts;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UNRECEIVED_WALL_MESSAGES;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_WALL_POSTS;

@Service
@Transactional
public class WallMessageServiceImpl implements WallMessageService {

    static Log log = LogFactory.getLog(WallMessageServiceImpl.class.getName());

    @Autowired
    UserDao userDao;

    @Autowired
    WallDao wallDao;

    @Autowired
    WallMessageDao wallMessageDao;

    @Autowired
    UserService userService;

    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    Global global;

    @Override
    public boolean addReceived(Long wallMessageid, Long userId) throws Exception {
        WallMessage wallMessage = wallMessageDao.getWallMessageById(wallMessageid);
        List<Long> userIds = new ArrayList<>();

        userIds.add(userId);


        return addReceived(wallMessage,userIds);
    }

    @Override
    public boolean addReceived(final WallMessage wallMessage, final List<Long> userIds) throws Exception {


        List<User> users = new ArrayList<>();
        for (Long userId : userIds) {
            users.add(userService.getUserById(userId));
        }

        if (wallMessage == null) {
            return false;
        }

        wallMessage.getUsersReceivedTheWallMsg().addAll(users);

        return wallMessageDao.updateWallMessage(wallMessage);
    }

    @Override
    public boolean removeUnReceived(final Long wallPostId, final Long userId) throws Exception {
        User user = userService.getUserById(userId);

        WallMessage wallPost = getWallMessageById(wallPostId);

        if (wallPost == null) {
            return false;
        }

        wallPost.removeUsersUnReceivedTheWallPost(user);

        return wallMessageDao.updateWallMessage(wallPost);
    }

    @Override
    public String getUnReceived(int limit, final Long userId) throws Exception {
        Set<WallPost> wallPosts = new HashSet<>();

        // get unread wall messages related to user's wall
        UnReceivedWallPosts unReceivedWallMessages = getUnReceivedWallPosts(userId);
        //put unread wall messages to status read
        for (WallPost wallPost : unReceivedWallMessages.getWallMessages()) {
            removeUnReceived(wallPost.getWallMessageId(), userId);

            //TODO it is late so look at this tomorrow
            List<Long> userIds = new ArrayList<>();
            userIds.add(userId);
            addReceived(wallMessageDao.getWallMessageById(wallPost.getWallMessageId()), userIds);

            //TODO check order
            if (wallPosts.size() < limit) {
                 wallPosts.add(wallPost);
            }
        }

        unReceivedWallMessages.setWallMessages(wallPosts);

        return jsonUtils.getGSONbyResponseObject(unReceivedWallMessages);
    }

    private Set<WallMessage> getWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception {
        return userDao.getWallPosts(userId, fromWallPostId, itemsPerPage, direction);
    }

    @Override
    public WallPosts getUserPosts(final Long userId, final Long loggedUserId, final Long lastLoadedWallPostId, int itemsPerPage, final Integer direction) throws Exception {
        if (userId == null || loggedUserId == null) {
            return null;
        }

        Set<WallMessage> wallMessages = getWallPosts(userId, lastLoadedWallPostId, itemsPerPage, direction);

        Set<WallPost> allWallPosts = convertToWallPosts(wallMessages);

        Set<WallPost> wallPosts = new TreeSet<>(new WallPostComp());

        wallPosts.addAll(allWallPosts);

        WallPosts result = new WallPosts();
        result.setWallPosts(wallPosts);
        result.setObjectType(MOT_WALL_POSTS);

        return result;
    }

    @Override
    public WallPosts getReceivedUserPosts(final Long userId, final Long lastLoadedWallPostId, int itemsPerPage, final Integer direction) throws Exception {
        if (userId == null) {
            return null;
        }

        final Set<WallMessage> wallMessages = getReceivedWallPosts(userId, lastLoadedWallPostId, itemsPerPage, direction);

        final Set<WallPost> allWallPosts = convertToWallPosts(wallMessages);

        final Set<WallPost> wallPosts = new TreeSet<>(new WallPostComp());

        wallPosts.addAll(allWallPosts);

        final WallPosts result = new WallPosts();
        result.setWallPosts(wallPosts);
        result.setObjectType(MOT_WALL_POSTS);

        return result;
    }

    private Set<WallMessage> getReceivedWallPosts(final Long userId, final Long fromWallPostId, int itemsPerPage, int direction) throws Exception {
        return userDao.getReceivedWallPosts(userId, fromWallPostId, itemsPerPage, direction);
    }

    private UnReceivedWallPosts getUnReceivedWallPosts(final Long userId) throws Exception {
        log.debug("getting UnReceivedWallMessages for user id: " + userId);
        if (userId == null) {
            return null;
        }

        User user = userService.getUserById(userId);

        UnReceivedWallPosts unReceivedWallMessages = new UnReceivedWallPosts();
        unReceivedWallMessages.setWallMessages(convertToWallPosts(user.getUnReceivedWallPosts()));

        //set message type to mark it for UI how to handle it
        unReceivedWallMessages.setObjectType(MOT_UNRECEIVED_WALL_MESSAGES);

        return unReceivedWallMessages;
    }

    @Override
    public BroadcastMessage prepareBroadcastMessage(String clientMessage, User user, MessageObjectType messageType) throws Exception {
        log.debug("prepareBroadcastMessage:  clientMessage = " + clientMessage + " userName = " + user.getFullName() + " messageType = " + messageType);
        BroadcastMessage broadcastMessage = null;
        switch (messageType) {
            case MOT_NEW_WALL_MESSAGE:
                broadcastMessage = prepareWallPost(clientMessage, user);
                break;
            case MOT_SEND_COMPLETED_WALL_POST:
                broadcastMessage = prepareCompletedWallPost(clientMessage, user);
                break;
        }
        log.debug("prepareBroadcastMessage:  broadcastMessage UserIds = " + broadcastMessage.getUserIds() + " getStoredMessageId = " + broadcastMessage.getStoredMessageId() + " getMessageBody = " + broadcastMessage.getMessageBody());
        return broadcastMessage;
    }

    private BroadcastMessage prepareWallPost(String clientMessage, User user) throws Exception {
        Set<Long> wallFollowerIds = null;

        WallPost wallPost = jsonUtils.getWallMessageUI(clientMessage);
        WallMessage storedWallMessage = saveWallMessage(user, wallPost);

        List<Long> userIds = new ArrayList<>();
        final Long wallPostId = wallPost.getWallMessageId();
        //if there is an attachment , than put this wall post to a wait list till complete
        if (wallPost.getAttachments().size() > 0) {
            PostponedWallPost postponedWallPost = new PostponedWallPost();
            wallPost.setWallMessageId(storedWallMessage.getMessageId());
            postponedWallPost.setWallPost(wallPost);
            global.getUnCompletedWallPostMap().put(storedWallMessage.getMessageId(), postponedWallPost);
            userIds.add(user.getUserId());
            addReceived(storedWallMessage, userIds);
        } else {

            wallFollowerIds = userService.getMyWallFollowerIds(user.getUserId());

            //include myself
            wallFollowerIds.add(user.getUserId());
            userIds.addAll(wallFollowerIds);
            addUnReceived(storedWallMessage, userIds);
        }

        log.debug("wallPostId = " + storedWallMessage.getMessageId());
        wallPost.setWallMessageId(storedWallMessage.getMessageId());
        wallPost.setFromUserId(storedWallMessage.getOwnerOfWallMessage().getUserId());
        wallPost.setCreated(storedWallMessage.getCreated());

        final String json = jsonUtils.getGSONbyResponseObject(wallPost);
        return new BroadcastMessage(wallFollowerIds, json, wallPostId);
    }

    private BroadcastMessage prepareCompletedWallPost(String clientMessage, User user) throws Exception {
        SendCompletedWallPostRequest sendCompletedWallPostRequest = jsonUtils.getSendCompletedWallPostRequest(clientMessage);
        final Long wallPostId = sendCompletedWallPostRequest.getWallPostId();

        WallPost wallPost = global.getUnCompletedWallPostMap().get(wallPostId).getWallPost();

        final String json = jsonUtils.getGSONbyResponseObject(wallPost);

        final Set<Long> wallFollowerIds = userService.getMyWallFollowerIds(user.getUserId());

        wallFollowerIds.remove(user.getUserId());
        //include myself
        //wallFollowerIds.add(userId);
        List<Long> userIds = new ArrayList<>();
        userIds.addAll(wallFollowerIds);
        addUnReceived(wallMessageDao.getWallMessageById(wallPost.getWallMessageId()), userIds);
        global.getUnCompletedWallPostMap().remove(wallPostId);

        return new BroadcastMessage(wallFollowerIds, json, wallPostId);
    }

    private WallMessage saveWallMessage(final User ownerOfWallMessage, final WallPost wallPost)  throws Exception {
        Wall currentWall = wallDao.getWallById(wallPost.getWallId());
        if (currentWall == null) {
            return null;
        }

        Date date = new Date();

        WallMessage newWallMessage = new WallMessage();
        newWallMessage.setMessageText(wallPost.getMessageText());
        newWallMessage.setOwnerOfWallMessage(ownerOfWallMessage);
        newWallMessage.setCreated(date);
        newWallMessage.setUpdated(date);
        newWallMessage.setWall(currentWall);
        return addWallMessage(newWallMessage);
    }

    protected boolean addUnReceived(final WallMessage wallMessage, final List<Long> userIds) throws Exception {
        if (userIds == null) {
            return false;
        }


        List<User> users = new ArrayList<>();
        for (Long userId : userIds) {
            users.add(userService.getUserById(userId));
        }

        if (wallMessage == null) {
            return false;
        }

        wallMessage.getUsersUnReceivedTheWallPost().addAll(users);

        return wallMessageDao.updateWallMessage(wallMessage);
    }

    private WallMessage addWallMessage(final WallMessage wallMessage) throws Exception {
        return wallMessageDao.addWallMessage(wallMessage);
    }

    private WallMessage getWallMessageById(final Long wallId) throws Exception {
        return wallMessageDao.getWallMessageById(wallId);
    }
}
