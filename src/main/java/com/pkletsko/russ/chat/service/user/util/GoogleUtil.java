package com.pkletsko.russ.chat.service.user.util;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.pkletsko.russ.chat.model.user.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 29.01.15
 * Time: 09:56
 * To change this template use File | Settings | File Templates.
 */
public class GoogleUtil {

    static Log log = LogFactory.getLog(GoogleUtil.class.getName());

    public static Userinfoplus getGoogleUserInfoPlus (final String accessToken) {
        Userinfoplus userInfo = null;

        GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
        Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName("Oauth2").build();
        try {
            userInfo = oauth2.userinfo().get().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userInfo;
    }

    public static User getUserFromGooglePlusUserInfo (final Userinfoplus googlePlusUserInfo) {
        User user = null;

        if (googlePlusUserInfo != null) {
            user = new User();
            user.setLoginId(googlePlusUserInfo.getId());
            user.setFirstName(googlePlusUserInfo.getGivenName());
            user.setLastName(googlePlusUserInfo.getFamilyName());
            user.setFullName(googlePlusUserInfo.getName());
            user.setEmail(googlePlusUserInfo.getEmail());
            user.setProfileIcon(googlePlusUserInfo.getPicture());
        }

        return user;
    }
}
