package com.pkletsko.russ.chat.service.user;

import com.google.api.services.oauth2.model.Userinfoplus;
import com.pkletsko.russ.chat.dao.user.UserDao;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.*;
import com.pkletsko.russ.chat.service.common.Converter;
import com.pkletsko.russ.chat.service.model.LoginSourceType;
import com.pkletsko.russ.chat.service.school.SchoolService;
import com.pkletsko.russ.chat.service.user.util.GoogleUtil;
import com.pkletsko.russ.chat.service.wall.*;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.user.*;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import static com.pkletsko.russ.chat.service.common.Converter.*;
import static com.pkletsko.russ.chat.service.common.InformationExtractor.*;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.*;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    protected Global global;

    @Autowired
    protected JSONUtils jsonUtils;

    @Autowired
    protected UserDao userDao;

    @Autowired
    protected WallService wallServices;

    @Autowired
    protected SchoolService schoolService;

    static Log log = LogFactory.getLog(UserServiceImpl.class.getName());



    @Override
    public SearchResult searchUser(final String searchQuery) throws Exception {
        SearchResult searchResult = new SearchResult();
        searchResult.setUserProfiles(Converter.convertToUserProfileSet(userDao.search(searchQuery)));
        searchResult.setObjectType(MOT_SEARCH);
        return searchResult;
    }

    @Override
    public UserProfile login(final String accessToken, final String loginSrc) throws Exception {
        User user = null;

        LoginSourceType loginSourceType = LoginSourceType.findByCode(Integer.valueOf(loginSrc));

        switch (loginSourceType) {
            case GOOGLE:
                Userinfoplus googlePlusUserInfo = GoogleUtil.getGoogleUserInfoPlus(accessToken);

                if (googlePlusUserInfo != null) {
                    user = userDao.getUserByLoginId(googlePlusUserInfo.getId());
                    if (user == null) {
                        //add a new user to db
                        user = addGoogleUser(googlePlusUserInfo);
                    }
                }
                break;
            case FACEBOOK:
                String graph = null;
                try {
                    String g = "https://graph.facebook.com/me?access_token=" + accessToken;
                    URL u = new URL(g);
                    URLConnection c = u.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    String inputLine;
                    StringBuffer b = new StringBuffer();
                    while ((inputLine = in.readLine()) != null)
                        b.append(inputLine + "\n");
                    in.close();
                    graph = b.toString();
                } catch (Exception e) {
                    System.out.println(e);
                    // an error occurred, handle this
                }

                String facebookId = "";
                String firstName = "";
                String lastName = "";
                String email = "";
                String name = "";
                try {
                    JSONObject json = new JSONObject(graph);
                    facebookId = json.getString("id");
                    firstName = json.getString("first_name");
                    name = json.getString("name");

                    lastName = json.getString("last_name");
                    email = json.getString("email");
                } catch (JSONException e) {
                    // an error occurred, handle this
                }

                user = userDao.getUserByLoginId(facebookId);
                if (user == null) {

                    User newUser = new User();
                    newUser.setLoginId(facebookId);
                    newUser.setFirstName(firstName);
                    newUser.setLastName(lastName);
                    newUser.setFullName(name);
                    newUser.setEmail(email);
                    newUser.setProfileIcon("https://graph.facebook.com/" + facebookId + "/picture");

                    //add a new user to db
                    user = addFacebookUser(newUser);
                }
                break;
        }

        return convertToUserProfile(user, false);
    }

    @Override
    public User getUserById(final Long userId) throws Exception {
        return userDao.getUserById(userId);
    }

    @Override
    public UserProfile getUserProfile(final Long userId) throws Exception {
        if (userId == null) {
            return null;
        }

        return convertToUserProfile(getUserById(userId), null);
    }

    @Override
    public UserProfile getUserProfile(final Long userId, final Long loggedUserId) throws Exception {
        if (userId == null || loggedUserId == null) {
            return null;
        }

        final User requestedUser = getUserById(userId);
        final User loggerUser = getUserById(loggedUserId);

        boolean followed = requestedUser.getMyWalls().iterator().next().getFollowingUsers().contains(loggerUser);

        return convertToUserProfile(getUserById(userId), followed);
    }

    @Override
    public Set<Long> getMyWallFollowerIds(final Long userId) throws Exception {
        log.debug("getting my wall followers for user id: " + userId);
        if (userId == null) {
            return new HashSet<>();
        }

        Set<Long> userIds = new HashSet<>();
        //find requested user by userID
        User user = getUserById(userId);

        if (user != null) {
            //get set of users who following current user
            //each user has only one his own wall, but this wall has many users who are following the wall
            Set<User> followerSet = getMyFollowers(user);

            // preparation of each UserProfile for each follower
            for (User follower : followerSet) {
                userIds.add(follower.getUserId());
            }
        }
        return userIds;
    }


    /**
     * Get set of UserProfile who are following after requested userId
     *
     * @param userId
     * @return Set<UserProfile>
     * @throws Exception
     */
    @Override
    public UserProfiles getFollowing(final Long loggedUserId, final Long userId, final Long lastLoadedUserId, int itemsPerPage, final Integer direction) throws Exception {
        log.debug("getting following for user id: " + userId);
        UserProfiles response = new UserProfiles();

        response.setObjectType(MOT_LIST_OF_USER_PROFILE);
        if (userId == null) {
            return response;
        }

        Set<UserProfile> profiles = new TreeSet<>(new UserProfileComparator());
        Set<User> followingUsers = getFollowingUsers(userId, lastLoadedUserId, itemsPerPage, direction);
        Set<User> followingUsersForLoggedUser = getAllFollowingUsers(loggedUserId);

        for (User followingUser : followingUsers) {
            Boolean followed = false;

            //checking if current user following the wall of user who following him
            for (User user : followingUsersForLoggedUser) {
                //they following each other
                if (user.getUserId() == followingUser.getUserId()) {
                    followed = true;
                }
            }

            profiles.add(convertToUserProfile(followingUser, followed));
        }

        response.setUserProfiles(profiles);
        return response;
    }

    /**
     * Get set of UserProfile who are followers of requested userId
     *
     * @param userId
     * @return Set<UserProfile>
     * @throws Exception
     */
    @Override
    public UserProfiles getFollowers(final Long loggedUserId, final Long userId, final Long lastLoadedUserId, int itemsPerPage, final Integer direction) throws Exception {
        log.debug("getting followers for user id: " + userId);
        UserProfiles response = new UserProfiles();

        response.setObjectType(MOT_LIST_OF_USER_PROFILE);
        if (userId == null) {
            return response;
        }

        Set<UserProfile> profiles = new TreeSet<>(new UserProfileComparator());
        Set<User> followerUsers = getFollowerUsers(userId, lastLoadedUserId, itemsPerPage, direction);
        Set<User> followingUsers = getAllFollowingUsers(loggedUserId);

        for (User followerUser : followerUsers) {
            Boolean followed = false;

            //checking if current user following the wall of user who following him
            for (User user : followingUsers) {
                //they following each other
                if (user.getUserId() == followerUser.getUserId()) {
                    followed = true;
                }
            }

            profiles.add(convertToUserProfile(followerUser, followed));
        }

        response.setUserProfiles(profiles);
        return response;
    }

    @Override
    public String getUserProfileUpdate(String clientMessage, User user) throws Exception {
        UserProfileEdit userProfileEdit = jsonUtils.getUserProfileEditRequest(clientMessage);

        if (userProfileEdit.getFirstName() != null) {
            user.setFirstName(userProfileEdit.getFirstName());
        }

        if (userProfileEdit.getLastName() != null) {
            user.setLastName(userProfileEdit.getLastName());
        }

        if (userProfileEdit.getFirstName() != null || userProfileEdit.getLastName() != null) {
            String newName = "";
            if (userProfileEdit.getFirstName() != null && userProfileEdit.getLastName() != null) {
                newName = userProfileEdit.getFirstName() + " " + userProfileEdit.getLastName();
            } else if (userProfileEdit.getFirstName() != null) {
                newName = userProfileEdit.getFirstName() + " " + user.getLastName();
            } else if (userProfileEdit.getLastName() != null) {
                newName = user.getFirstName() + " " + userProfileEdit.getLastName();
            }

            user.setFullName(newName);
        }

        if (userProfileEdit.getNickName() != null) {
            user.setNickName(userProfileEdit.getNickName());
        }

        if (userProfileEdit.getDateOfBirth() != null) {
            user.setBirthDate(userProfileEdit.getDateOfBirth());
        }

        if (userProfileEdit.getCountry() != null) {
            user.setCountry(userProfileEdit.getCountry());
        }

        if (userProfileEdit.getCity() != null) {
            user.setCity(userProfileEdit.getCity());
        }

        if (userProfileEdit.getSchoolClass() != null) {
            user.setSchoolClass(schoolService.getSchoolClassById(userProfileEdit.getSchoolClass()));
        }

        if (userProfileEdit.getColour() != null) {
            user.setColour(userProfileEdit.getColour());
        }

        user.setVersion(user.getVersion() + 1);

        updateUser(user);

        UserProfiles userProfiles = new UserProfiles();
        Set<UserProfile> userProfileSet = new HashSet<>();
        userProfileSet.add(getUserProfile(user.getUserId()));
        userProfiles.setUserProfiles(userProfileSet);
        userProfiles.setObjectType(MOT_UPDATE_USER_PROFILE_INFO);
        return jsonUtils.getGSONbyResponseObject(userProfiles);
    }

    @Override
    public String getUserProfileResponse(String clientMessage, User user) throws Exception {
        UserProfileRequest userProfileRequest = jsonUtils.getUserProfileRequest(clientMessage);
        UserProfile userProfile = getUserProfile(userProfileRequest.getUserId(), user.getUserId());
        UserProfileResponse userProfileResponse = new UserProfileResponse();
        userProfileResponse.setUserProfile(userProfile);
        userProfileResponse.setObjectType(MOT_GET_USER_PROFILE);
        return jsonUtils.getGSONbyResponseObject(userProfileResponse);
    }

    @Override
    public String getSearchResult(String clientMessage) throws Exception {
        SearchRequest searchRequest = jsonUtils.getSearchRequest(clientMessage);
        SearchResult searchResult = searchUser(searchRequest.getSearchQuery());
        return jsonUtils.getGSONbyResponseObject(searchResult);
    }

    private User addUser(final User user) throws Exception {
        return userDao.addUser(user);
    }

    private boolean updateUser(final User user) throws Exception {
        return userDao.updateUser(user);
    }

    private Set<User> getFollowingUsers(final Long userId, final Long lastLoadedUserId, int itemsPerPage, int direction) throws Exception {
        return userDao.getFollowing(userId, lastLoadedUserId, itemsPerPage, direction);
    }

    private Set<User> getAllFollowingUsers(final Long userId) throws Exception {
        return userDao.getAllFollowing(userId);
    }

    private Set<User> getFollowerUsers(final Long userId, final Long lastLoadedUserId, int itemsPerPage, int direction) throws Exception {
        return userDao.getFollowers(userId, lastLoadedUserId, itemsPerPage, direction);
    }

    private User addGoogleUser(final Userinfoplus googlePlusUserInfo) throws Exception {
        if (googlePlusUserInfo == null) {
            return null;
        }

        log.debug("adding google user " + googlePlusUserInfo.getName());

        User user = GoogleUtil.getUserFromGooglePlusUserInfo(googlePlusUserInfo);
        user.setUserTypeId(1);

        //save user to Database
        user = addUser(user);

        //create user wall (only one for each user)
        Wall myWall = wallServices.addWall(user);
        Set<Wall> myWalls = new HashSet<>();
        myWalls.add(myWall);
        user.setMyWalls(myWalls);

        return user;
    }

    private User addFacebookUser(User user) throws Exception {
        if (user == null) {
            return null;
        }

        log.debug("adding facebook user " + user.getFullName());

        user.setUserTypeId(1);

        //save user to Database
        user = addUser(user);

        //create user wall (only one for each user)
        Wall myWall = wallServices.addWall(user);
        Set<Wall> myWalls = new HashSet<>();
        myWalls.add(myWall);
        user.setMyWalls(myWalls);

        return user;
    }


}
