package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.dao.wall.CommentHistoryDao;
import com.pkletsko.russ.chat.dao.wall.WallMessageDao;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.CommentHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.CommentComp;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comments;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedComments;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static com.pkletsko.russ.chat.service.common.Converter.convertToComment;
import static com.pkletsko.russ.chat.service.common.Converter.convertToCommentSet;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LIST_OF_USER_PROFILE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UNRECEIVED_COMMENTS;

@Service
@Transactional
public class CommentHistoryServiceImpl implements CommentHistoryService {

    static Log log = LogFactory.getLog(CommentHistoryServiceImpl.class.getName());

    @Autowired
    UserService userService;

    @Autowired
    WallMessageDao wallMessageDao;

    @Autowired
    CommentHistoryDao commentHistoryDao;

    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    Global global;

    @Override
    public boolean addReceived(final Long commentHistoryId, final Long userId) throws Exception {
        User user = userService.getUserById(userId);

        CommentHistory commentHistory = getCommentHistoryById(commentHistoryId);

        if (commentHistory == null) {
            return false;
        }

        commentHistory.addUsersReceivedTheComment(user);

        return commentHistoryDao.updateCommentHistory(commentHistory);
    }

    @Override
    public boolean removeUnReceived(final Long commentHistoryId, final Long userId) throws Exception {
        User user = userService.getUserById(userId);

        CommentHistory commentHistory = getCommentHistoryById(commentHistoryId);

        if (commentHistory == null) {
            return false;
        }

        commentHistory.removeUsersUnReceivedTheComment(user);

        return commentHistoryDao.updateCommentHistory(commentHistory);
    }

    @Override
    public String getUnReceived(Long userId) throws Exception {
        UnReceivedComments unReceivedComments = getUnReceivedComments(userId);
        for (Comment comment : unReceivedComments.getComments()) {
            removeUnReceived(comment.getCommentId(), userId);
            addReceived(comment.getCommentId(), userId);
        }

        return jsonUtils.getGSONbyResponseObject(unReceivedComments);
    }

    @Override
    public BroadcastMessage prepareBroadcastMessage(String clientMessage, User user) throws Exception {
        Comment comment = jsonUtils.getComment(clientMessage);
        comment = saveCommentHistory(user, comment);

        final Long wallPostOwnerId = getWallPostOwnerId(comment.getWallMessageId());

        final String json = jsonUtils.getGSONbyResponseObject(comment);
        log.debug("sendCommentToAllActiveFollowers: to followers of userID = " + wallPostOwnerId + ", from = " + user.getUserId() + " , " + json);

        final Set<Long> wallFollowerIds = userService.getMyWallFollowerIds(wallPostOwnerId);

        //include myself
        wallFollowerIds.add(wallPostOwnerId);
        wallFollowerIds.add(user.getUserId());

        for (final Long followerId : wallFollowerIds) {
           addUnReceived(comment.getCommentId(), followerId);
        }

        return new BroadcastMessage(wallFollowerIds, json, comment.getCommentId());
    }

    private Set<CommentHistory> getComments(Long wallPostId, Long lastLoadedCommentId, int itemsPerPage, int direction) throws Exception {
        return wallMessageDao.getComments(wallPostId, lastLoadedCommentId, itemsPerPage, direction);
    }

    @Override
    public Comments getComments(final Long wallPostId, final Long lastLoadedCommentId, int itemsPerPage, final Long userId, final Integer direction) throws Exception {
        log.debug("getting comments for wall post id: " + wallPostId);
        Comments response = new Comments();
        response.setObjectType(MOT_LIST_OF_USER_PROFILE);

        if (wallPostId == null) {
            return response;
        }

        final Set<Comment> result = new TreeSet<>(new CommentComp());
        final Set<UserProfile> userProfileSet = new HashSet<>();

        final Set<CommentHistory> comments = getComments(wallPostId, lastLoadedCommentId, itemsPerPage, direction);

        // create an iterator
        final Iterator iterator = comments.iterator();

        // check values
        while (iterator.hasNext() && itemsPerPage != 0) {
            //TODO think about to sent minimal user details
            final CommentHistory commentHistory = (CommentHistory) iterator.next();
            result.add(convertToComment(commentHistory));
            userProfileSet.add(userService.getUserProfile(commentHistory.getOwnerOfComment().getUserId(), userId));
        }

        response.setComments(result);
        response.setUserProfiles(userProfileSet);

        return response;
    }

    private UnReceivedComments getUnReceivedComments(final Long userId) throws Exception {
        log.debug("getting UnReceivedComments for user id: " + userId);
        if (userId == null) {
            return null;
        }

        User user = userService.getUserById(userId);

        UnReceivedComments unReceivedComments = new UnReceivedComments();
        unReceivedComments.setComments(convertToCommentSet(user.getUnReceivedComments()));
        unReceivedComments.setObjectType(MOT_UNRECEIVED_COMMENTS);

        return unReceivedComments;
    }

    private Comment saveCommentHistory(final User ownerOfCommentHistory, final Comment comment)  throws Exception {
        CommentHistory newCommentHistory = new CommentHistory();
        newCommentHistory.setOwnerOfComment(ownerOfCommentHistory);
        newCommentHistory.setCommentText(comment.getCommentText());

        comment.setCommentId(addCommentHistory(newCommentHistory, comment.getWallMessageId()));
        comment.setFromUserId(ownerOfCommentHistory.getUserId());

        return comment;
    }

    private boolean addUnReceived(final Long commentHistoryId, final Long userId) throws Exception {
        if (userId == null) {
            return false;
        }

        User user = userService.getUserById(userId);
        //TODO error handling

        CommentHistory commentHistory = getCommentHistoryById(commentHistoryId);

        if (commentHistory == null) {
            return false;
        }

        commentHistory.addUsersUnReceivedTheComment(user);

        return commentHistoryDao.updateCommentHistory(commentHistory);
    }

    private Long addCommentHistory(final CommentHistory commentHistory, final Long wallMessageId) throws Exception {
        WallMessage currentWallMessage = wallMessageDao.getWallMessageById(wallMessageId);
        if (currentWallMessage == null) {
            return null;
        }

        commentHistory.setWallMessage(currentWallMessage);

        return commentHistoryDao.addCommentHistory(commentHistory);
    }

    private CommentHistory getCommentHistoryById(final Long wallId) throws Exception {
        return commentHistoryDao.getCommentHistoryById(wallId);
    }

    private Long getWallPostOwnerId(final Long wallPostId) throws Exception {
        return wallMessageDao.getWallMessageById(wallPostId).getOwnerOfWallMessage().getUserId();
    }

}
