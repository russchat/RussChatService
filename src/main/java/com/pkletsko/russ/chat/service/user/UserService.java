package com.pkletsko.russ.chat.service.user;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.UnReceivedChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfiles;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;

import java.util.Set;

public interface UserService {

    /**
     * Server side objects
     */

    User getUserById(final Long userId) throws Exception;

    Set<Long> getMyWallFollowerIds(final Long userId) throws Exception;

    /**
     * Wrapped for client
     */
    UserProfile getUserProfile(final Long userId) throws Exception;

    UserProfile getUserProfile(final Long userId, final Long loggedUserId) throws Exception;


    /**
     * Refactored with criteria methods
     */

    UserProfiles getFollowers(final Long loggedUserId, final Long userId, final Long lastLoadedUserId, int itemsPerPage, final Integer direction) throws Exception;

    UserProfiles getFollowing(final Long loggedUserId, final Long userId, final Long lastLoadedUserId, int itemsPerPage, final Integer direction) throws Exception;

    /**
     * Search methods
     */
    SearchResult searchUser(final String searchQuery) throws Exception;

    /**
     * Login methods
     */
    UserProfile login(final String accessToken, final String loginSrc) throws Exception;


    String getUserProfileUpdate(final String clientMessage, User user) throws Exception;

    String getUserProfileResponse(final String clientMessage, User user) throws Exception;

    String getSearchResult(String clientMessage) throws Exception;

}
