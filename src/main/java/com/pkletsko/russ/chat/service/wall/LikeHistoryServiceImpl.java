package com.pkletsko.russ.chat.service.wall;

import com.pkletsko.russ.chat.dao.wall.LikeHistoryDao;
import com.pkletsko.russ.chat.dao.wall.WallMessageDao;
import com.pkletsko.russ.chat.service.common.Converter;
import com.pkletsko.russ.chat.service.model.BroadcastMessage;
import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.model.wall.LikeHistory;
import com.pkletsko.russ.chat.model.wall.WallMessage;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.LikeComp;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Likes;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedLikes;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.pkletsko.russ.chat.service.common.Converter.convertToLike;
import static com.pkletsko.russ.chat.service.common.Converter.convertToLikeSet;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LIST_OF_USER_PROFILE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UNRECEIVED_LIKES;

@Service
@Transactional
public class LikeHistoryServiceImpl implements LikeHistoryService {

    static Log log = LogFactory.getLog(LikeHistoryServiceImpl.class.getName());

    @Autowired
    UserService userService;

    @Autowired
    WallMessageDao wallMessageDao;

    @Autowired
    LikeHistoryDao likeHistoryDao;

    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    Global global;

    @Override
    public boolean addReceived(final Long likeHistoryId, final Long userId) throws Exception {
        log.debug("addReceived likeHistoryId = " + likeHistoryId + " user = " + userId);

        User user  = userService.getUserById(userId);

        LikeHistory likeHistory = getLikeHistoryById(likeHistoryId);

        if (likeHistory == null) {
            return false;
        }

        likeHistory.addUsersReceivedTheLike(user);

        Boolean result = updateLikeHistory(likeHistory);

        log.debug("addReceived result = " + result);
        return result;
    }

    @Override
    public boolean removeUnReceived(final Long likeHistoryId, final Long userId) throws Exception {
        User user  = userService.getUserById(userId);

        LikeHistory likeHistory = getLikeHistoryById(likeHistoryId);

        if (likeHistory == null) {
            return false;
        }

        likeHistory.removerUsersUnReceivedTheLike(user);

        return updateLikeHistory(likeHistory);
    }

    @Override
    public String getUnReceived(Long userId) throws Exception {
        UnReceivedLikes unReceivedLikes = getUnReceivedLikes(userId);
        for (Like like : unReceivedLikes.getLikes()) {
            removeUnReceived(like.getLikeId(), userId);
            addReceived(like.getLikeId(), userId);
        }
        return jsonUtils.getGSONbyResponseObject(unReceivedLikes);
    }



    @Override
    public BroadcastMessage prepareBroadcastMessage(String clientMessage, User user, MessageObjectType messageType) throws Exception {
        log.debug("prepareBroadcastMessage:  clientMessage = " + clientMessage + " userName = " + user.getFullName() + " messageType = " + messageType);
        BroadcastMessage broadcastMessage = null;
        switch (messageType) {
            case MOT_LIKE:
                broadcastMessage = prepareLike(clientMessage, user);
                break;
            case MOT_DISLIKE:
                broadcastMessage = prepareDislike(clientMessage, user);
                break;
        }
        log.debug("prepareBroadcastMessage:  broadcastMessage UserIds = " + broadcastMessage.getUserIds() + " getStoredMessageId = " + broadcastMessage.getStoredMessageId() + " getMessageBody = " + broadcastMessage.getMessageBody());
        return broadcastMessage;
    }

    @Override
    public Likes getLikes(final Long wallPostId, final Long lastLoadedUserLikedId, int itemsPerPage, final Long userId, final Integer direction) throws Exception {
        log.debug("getting likes for wall post id: " + wallPostId);
        Likes response = new Likes();
        response.setObjectType(MOT_LIST_OF_USER_PROFILE);

        if (wallPostId == null) {
            return response;
        }

        //final Set<Like> result = new TreeSet<>(new LikeComp());
        final Set<UserProfile> userProfileSet = new HashSet<>();
              List<Like> likesTemp = new ArrayList<>();
        final Set<LikeHistory> likes = getLikes(wallPostId, lastLoadedUserLikedId, itemsPerPage, direction);

        for (LikeHistory likeHistory : likes) {
            final Like like = Converter.convertToLike(likeHistory);
            likesTemp.add(like);
            userProfileSet.add(userService.getUserProfile(likeHistory.getOwnerOfLike().getUserId(), userId));
        }

        response.getLikes().addAll(likesTemp);
        response.setUserProfiles(userProfileSet);

        return response;
    }

    private Set<LikeHistory> getLikes(Long wallPostId, Long lastLoadedLikeId, int itemsPerPage, int direction) throws Exception {
        return wallMessageDao.getLikes(wallPostId, lastLoadedLikeId, itemsPerPage, direction);
    }

    private BroadcastMessage prepareLike(String clientMessage, User user) throws Exception {
        Like like = jsonUtils.getLike(clientMessage);
        like = like(user, like);
        final String json = jsonUtils.getGSONbyResponseObject(like);

        final Long wallPostOwnerId = getWallPostOwnerId(like.getWallMessageId());

        log.debug("prepareLike: wallPostOwnerId = " + wallPostOwnerId + ", from = " + user.getUserId() + " , " + json);

        final Set<Long> wallFollowerIds = userService.getMyWallFollowerIds(wallPostOwnerId);

        //include myself
        wallFollowerIds.add(wallPostOwnerId);
        wallFollowerIds.add(user.getUserId());

        for (final Long followerId : wallFollowerIds) {
            addUnReceived(like.getLikeId(), followerId);
        }

        return new BroadcastMessage(wallFollowerIds, json, like.getLikeId());
    }

    private BroadcastMessage prepareDislike(String clientMessage, User user) throws Exception {
        Like like = jsonUtils.getLike(clientMessage);
        unlike(like);

        final Long wallPostOwnerId = getWallPostOwnerId(like.getWallMessageId());

        final String json = jsonUtils.getGSONbyResponseObject(like);
        log.debug("sendDislikeToAllActiveFollowers: to followers of userID = " + wallPostOwnerId + ", from = " + user.getUserId() + " , " + json);

        final Set<Long> wallFollowerIds = userService.getMyWallFollowerIds(wallPostOwnerId);

        //include myself
        wallFollowerIds.add(wallPostOwnerId);
        wallFollowerIds.add(user.getUserId());

        for (final Long followerId : wallFollowerIds) {
            addUnReceived(like.getLikeId(), followerId);
        }

        return new BroadcastMessage(wallFollowerIds, json, like.getLikeId());
    }

    private Like like(final User ownerOfLikeHistory, final Like like)  throws Exception {
        final Long userId = ownerOfLikeHistory.getUserId();
        final Long wallPostId = like.getWallMessageId();

        LikeHistory existedLike = getLikeHistory(userId, wallPostId);

        if (existedLike == null) {
            LikeHistory newLikeHistory = new LikeHistory();
            newLikeHistory.setOwnerOfLike(ownerOfLikeHistory);
            newLikeHistory.setDisabled(like.getDisabled());

            like.setLikeId(addLikeHistory(newLikeHistory, wallPostId));
            like.setFromUserId(userId);

            return like;
        }

        existedLike.setDisabled(false);
        updateLikeHistory(existedLike);

        return convertToLike(existedLike);
    }

    private boolean unlike(final Like like) throws Exception {
        if (like == null) {
            return false;
        }

        LikeHistory likeHistory = getLikeHistory(like.getFromUserId(), like.getWallMessageId());

        if (likeHistory == null) {
            return false;
        }

        likeHistory.setDisabled(true);

        return updateLikeHistory(likeHistory);
    }

    private boolean addUnReceived(final Long likeHistoryId, final Long userId) throws Exception {
        if (userId == null) {
            return false;
        }

        User user = userService.getUserById(userId);
        //TODO error handling

        LikeHistory likeHistory = getLikeHistoryById(likeHistoryId);

        if (likeHistory == null) {
            return false;
        }

        likeHistory.addUsersUnReceivedTheLike(user);

        return updateLikeHistory(likeHistory);
    }

    private Long addLikeHistory(final LikeHistory likeHistory, final Long wallMessageId) throws Exception {
        WallMessage currentWallMessage = wallMessageDao.getWallMessageById(wallMessageId);
        if (currentWallMessage == null) {
            return null;
        }

        likeHistory.setWallMessage(currentWallMessage);

        return likeHistoryDao.addLikeHistory(likeHistory);
    }

    private LikeHistory getLikeHistoryById(final Long wallId) throws Exception {
        return likeHistoryDao.getLikeHistoryById(wallId);
    }

    private LikeHistory getLikeHistory(final Long userId, final Long wallPostId) throws Exception {
        return likeHistoryDao.getLikeHistory(userId, wallPostId);
    }

    private boolean updateLikeHistory(LikeHistory likeHistory) throws Exception{
        return likeHistoryDao.updateLikeHistory(likeHistory);
    }

    private Long getWallPostOwnerId(final Long wallPostId) throws Exception {
        return wallMessageDao.getWallMessageById(wallPostId).getOwnerOfWallMessage().getUserId();
    }

    private UnReceivedLikes getUnReceivedLikes(final Long userId) throws Exception {
        log.debug("getting UnReceivedLikes for user id: " + userId);
        if (userId == null) {
            return null;
        }

        User user = userService.getUserById(userId);

        UnReceivedLikes unReceivedLikes = new UnReceivedLikes();
        unReceivedLikes.setLikes(convertToLikeSet(user.getUnReceivedLikes()));
        unReceivedLikes.setObjectType(MOT_UNRECEIVED_LIKES);

        return unReceivedLikes;
    }
}
