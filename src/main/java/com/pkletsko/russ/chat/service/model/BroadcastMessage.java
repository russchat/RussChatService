package com.pkletsko.russ.chat.service.model;

import java.util.HashSet;
import java.util.Set;

public class BroadcastMessage {
    private String messageBody;
    private Set<Long> userIds = new HashSet<>();
    private Long storedMessageId;

    public BroadcastMessage(Set<Long> userIds, String messageBody, Long storedMessageId) {
        this.userIds = userIds;
        this.messageBody = messageBody;
        this.storedMessageId = storedMessageId;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public Long getStoredMessageId() {
        return storedMessageId;
    }
}
