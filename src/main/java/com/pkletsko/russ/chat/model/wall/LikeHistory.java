package com.pkletsko.russ.chat.model.wall;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "like_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LikeHistory implements Serializable {

	private static final long serialVersionUID = 3L;

	@Id
	@GeneratedValue
	@Column(name = "likeId")
	private Long likeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallMessageId", nullable = false)
    private WallMessage wallMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfLike;

    @Column(name = "disabled")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean disabled;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "received_like_history", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "likeId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersReceivedTheLike = new HashSet<User>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "unreceived_like_history", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "likeId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersUnReceivedTheLike = new HashSet<User>();

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Set<User> getUsersUnReceivedTheLike() {
        return usersUnReceivedTheLike;
    }

    public void setUsersUnReceivedTheLike(Set<User> usersUnReceivedTheLike) {
        this.usersUnReceivedTheLike = usersUnReceivedTheLike;
    }


    public Set<User> getUsersReceivedTheLike() {
        return usersReceivedTheLike;
    }

    public void addUsersReceivedTheLike(User user) {
        boolean needToAdd = true;
        for (User user1 : usersReceivedTheLike) {
             if (user1.equals(user)) {
                 needToAdd = false;
             }
        }

        if (needToAdd) {
            this.usersReceivedTheLike.add(user);
        }
    }

    public void addUsersUnReceivedTheLike(User user) {
        boolean needToAdd = true;
        for (User user1 : usersUnReceivedTheLike) {
            if (user1.equals(user)) {
                needToAdd = false;
            }
        }

        if (needToAdd) {
            this.usersUnReceivedTheLike.add(user);
        }
    }

    public void removerUsersUnReceivedTheLike(User user) {
        this.usersUnReceivedTheLike.remove(user);
    }

    public void setUsersReceivedTheLike(Set<User> usersReceivedTheLike) {
        this.usersReceivedTheLike = usersReceivedTheLike;
    }

    public Long getLikeId() {
        return likeId;
    }

    public void setLikeId(Long likeId) {
        this.likeId = likeId;
    }

    public WallMessage getWallMessage() {
        return wallMessage;
    }

    public void setWallMessage(WallMessage wallMessage) {
        this.wallMessage = wallMessage;
    }

    public User getOwnerOfLike() {
        return ownerOfLike;
    }

    public void setOwnerOfLike(User ownerOfLike) {
        this.ownerOfLike = ownerOfLike;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikeHistory that = (LikeHistory) o;

        if (disabled != that.disabled) return false;
        if (likeId != null ? !likeId.equals(that.likeId) : that.likeId != null) return false;
        if (wallMessage != null ? !wallMessage.equals(that.wallMessage) : that.wallMessage != null) return false;
        if (ownerOfLike != null ? !ownerOfLike.equals(that.ownerOfLike) : that.ownerOfLike != null) return false;
        if (usersReceivedTheLike != null ? !usersReceivedTheLike.equals(that.usersReceivedTheLike) : that.usersReceivedTheLike != null)
            return false;
        return !(usersUnReceivedTheLike != null ? !usersUnReceivedTheLike.equals(that.usersUnReceivedTheLike) : that.usersUnReceivedTheLike != null);

    }

    @Override
    public int hashCode() {
        int result = likeId != null ? likeId.hashCode() : 0;
//        result = 31 * result + (wallMessage != null ? wallMessage.hashCode() : 0);
//        result = 31 * result + (ownerOfLike != null ? ownerOfLike.hashCode() : 0);
        result = 31 * result + (disabled ? 1 : 0);
//        result = 31 * result + (usersReceivedTheLike != null ? usersReceivedTheLike.hashCode() : 0);
//        result = 31 * result + (usersUnReceivedTheLike != null ? usersUnReceivedTheLike.hashCode() : 0);
        return result;
    }
}
