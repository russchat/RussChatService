package com.pkletsko.russ.chat.model.conversation;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "conversation", catalog = "russchat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Conversation implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue
	@Column(name = "conversationId")
	private Long conversationId;

	@Column(name = "conversationName")
	private String conversationName;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "conversation_user", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "conversationId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> users = new HashSet<User>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "conversation")
    private Set<ConversationMessage> messages = new HashSet<ConversationMessage>();

    public Set<ConversationMessage> getMessages() {
        return messages;
    }

    public void setMessages(Set<ConversationMessage> messages) {
        this.messages = messages;
    }

    public void addMessage(ConversationMessage message) {
        this.messages.add(message);
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Conversation that = (Conversation) o;

        if (conversationId != null ? !conversationId.equals(that.conversationId) : that.conversationId != null)
            return false;
        if (conversationName != null ? !conversationName.equals(that.conversationName) : that.conversationName != null)
            return false;
        if (users != null ? !users.equals(that.users) : that.users != null) return false;
        return !(messages != null ? !messages.equals(that.messages) : that.messages != null);

    }

    @Override
    public int hashCode() {
        int result = conversationId != null ? conversationId.hashCode() : 0;
        result = 31 * result + (conversationName != null ? conversationName.hashCode() : 0);
//        result = 31 * result + (users != null ? users.hashCode() : 0);
//        result = 31 * result + (messages != null ? messages.hashCode() : 0);
        return result;
    }
}
