package com.pkletsko.russ.chat.model.user;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class UserComparator implements Comparator<User> {
    @Override
    public int compare(User e1, User e2) {
        final long id1 = e1.getUserId(), id2 = e2.getUserId();
        if (id1 == id2)
            return 0;
        return id1 > id2 ? 1 : -1;
    }
}
