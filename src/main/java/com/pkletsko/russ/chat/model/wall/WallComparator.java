package com.pkletsko.russ.chat.model.wall;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class WallComparator implements Comparator<Wall> {
    @Override
    public int compare(Wall e1, Wall e2) {
        final long id1 = e1.getWallId(), id2 = e2.getWallId();
        if (id1 == id2)
            return 0;
        return id1 > id2 ? 1 : -1;
    }
}
