package com.pkletsko.russ.chat.model.wall;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "wall_message")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class WallMessage implements Serializable {

	private static final long serialVersionUID = 3L;

	@Id
	@GeneratedValue
	@Column(name = "wallMessageId")
	private Long messageId;

	@Column(name = "messageText")
	private String messageText;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallId", nullable = false)
    private Wall wall;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfWallMessage;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "received_wall_message", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "wallMessageId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersReceivedTheWallMsg = new HashSet<User>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallMessage")
    private Set<LikeHistory> likes = new HashSet<LikeHistory>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallMessage")
    private Set<CommentHistory> comments = new HashSet<CommentHistory>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallMessage")
    private Set<AttachmentHistory> attachments = new HashSet<AttachmentHistory>();

    @Column(name = "created" , columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "updated" , columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "unreceived_wall_post_history", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "wallMessageId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersUnReceivedTheWallPost = new HashSet<User>();

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Set<CommentHistory> getComments() {
        return comments;
    }

    public void setComments(Set<CommentHistory> comments) {
        this.comments = comments;
    }

    public Set<LikeHistory> getLikes() {
        return likes;
    }

    public void setLikes(Set<LikeHistory> likes) {
        this.likes = likes;
    }

    public void addUsersReceivedTheWallMsg(User user) {
        boolean needToAdd = true;
        for (User user1 : usersReceivedTheWallMsg) {
            if (user1.equals(user)) {
                needToAdd = false;
            }
        }

        if (needToAdd) {
            this.usersReceivedTheWallMsg.add(user);
        }
    }

    public void removeUsersReceivedTheWallMsg(User user) {
        this.usersReceivedTheWallMsg.remove(user);
    }

    public void addUsersUnReceivedTheWallPost(User user) {
        boolean needToAdd = true;
        for (User user1 : usersUnReceivedTheWallPost) {
            if (user1.equals(user)) {
                needToAdd = false;
            }
        }

        if (needToAdd) {
            this.usersUnReceivedTheWallPost.add(user);
        }
    }

    public void removeUsersUnReceivedTheWallPost(User user) {
        this.usersUnReceivedTheWallPost.remove(user);
    }

    public Set<User> getUsersUnReceivedTheWallPost() {
        return usersUnReceivedTheWallPost;
    }

    public void setUsersUnReceivedTheWallPost(Set<User> usersUnReceivedTheWallPost) {
        this.usersUnReceivedTheWallPost = usersUnReceivedTheWallPost;
    }

    public Wall getWall() {
        return wall;
    }

    public void setWall(Wall wall) {
        this.wall = wall;
    }

    public User getOwnerOfWallMessage() {
        return ownerOfWallMessage;
    }

    public void setOwnerOfWallMessage(User ownerOfWallMessage) {
        this.ownerOfWallMessage = ownerOfWallMessage;
    }

    public Set<User> getUsersReceivedTheWallMsg() {
        return usersReceivedTheWallMsg;
    }

    public void setUsersReceivedTheWallMsg(Set<User> usersReceivedTheWallMsg) {
        this.usersReceivedTheWallMsg = usersReceivedTheWallMsg;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Set<AttachmentHistory> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<AttachmentHistory> attachments) {
        this.attachments = attachments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WallMessage that = (WallMessage) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;
        if (messageText != null ? !messageText.equals(that.messageText) : that.messageText != null) return false;
        if (wall != null ? !wall.equals(that.wall) : that.wall != null) return false;
        if (ownerOfWallMessage != null ? !ownerOfWallMessage.equals(that.ownerOfWallMessage) : that.ownerOfWallMessage != null)
            return false;
        if (usersReceivedTheWallMsg != null ? !usersReceivedTheWallMsg.equals(that.usersReceivedTheWallMsg) : that.usersReceivedTheWallMsg != null)
            return false;
        if (likes != null ? !likes.equals(that.likes) : that.likes != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (attachments != null ? !attachments.equals(that.attachments) : that.attachments != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        return !(usersUnReceivedTheWallPost != null ? !usersUnReceivedTheWallPost.equals(that.usersUnReceivedTheWallPost) : that.usersUnReceivedTheWallPost != null);

    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (messageText != null ? messageText.hashCode() : 0);
//        result = 31 * result + (wall != null ? wall.hashCode() : 0);
//        result = 31 * result + (ownerOfWallMessage != null ? ownerOfWallMessage.hashCode() : 0);
//        result = 31 * result + (usersReceivedTheWallMsg != null ? usersReceivedTheWallMsg.hashCode() : 0);
//        result = 31 * result + (likes != null ? likes.hashCode() : 0);
//        result = 31 * result + (comments != null ? comments.hashCode() : 0);
//        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
//        result = 31 * result + (usersUnReceivedTheWallPost != null ? usersUnReceivedTheWallPost.hashCode() : 0);
        return result;
    }
}
