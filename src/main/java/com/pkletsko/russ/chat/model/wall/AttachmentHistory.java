package com.pkletsko.russ.chat.model.wall;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "attachment", catalog = "russchat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AttachmentHistory implements Serializable {

    private static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue
    @Column(name = "attachmentId")
    private Long attachmentId;

    @Column(name = "attachmentName")
    private String attachmentName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallMessageId", nullable = false)
    private WallMessage wallMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfAttachment;

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public WallMessage getWallMessage() {
        return wallMessage;
    }

    public void setWallMessage(WallMessage wallMessage) {
        this.wallMessage = wallMessage;
    }

    public User getOwnerOfAttachment() {
        return ownerOfAttachment;
    }

    public void setOwnerOfAttachment(User ownerOfAttachment) {
        this.ownerOfAttachment = ownerOfAttachment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttachmentHistory that = (AttachmentHistory) o;

        if (attachmentId != null ? !attachmentId.equals(that.attachmentId) : that.attachmentId != null) return false;
        if (attachmentName != null ? !attachmentName.equals(that.attachmentName) : that.attachmentName != null)
            return false;
        if (wallMessage != null ? !wallMessage.equals(that.wallMessage) : that.wallMessage != null) return false;
        return !(ownerOfAttachment != null ? !ownerOfAttachment.equals(that.ownerOfAttachment) : that.ownerOfAttachment != null);

    }

    @Override
    public int hashCode() {
        int result = attachmentId != null ? attachmentId.hashCode() : 0;
        result = 31 * result + (attachmentName != null ? attachmentName.hashCode() : 0);
        result = 31 * result + (wallMessage != null ? wallMessage.hashCode() : 0);
//        result = 31 * result + (ownerOfAttachment != null ? ownerOfAttachment.hashCode() : 0);
        return result;
    }
}
