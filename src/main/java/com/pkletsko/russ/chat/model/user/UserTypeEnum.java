package com.pkletsko.russ.chat.model.user;

/**
 * User: pkletsko
 * Date: 4/10/14
 * Time: 11:07 AM
 */
public enum UserTypeEnum {
    unknown(1);

    private int userTypeId;

    private UserTypeEnum(final int userTypeCode) {
        userTypeId = userTypeCode;
    }

    public int getUserTypeId() {
        return userTypeId;
    }
}
