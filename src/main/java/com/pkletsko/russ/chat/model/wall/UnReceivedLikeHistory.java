package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "unreceived_like_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UnReceivedLikeHistory implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "unReceivedLikeId")
    private Long unReceivedLikeId;

	@Column(name = "likeId")
	private Long likeId;

    @Column(name = "userId")
    private Long userId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUnReceivedLikeId() {
        return unReceivedLikeId;
    }

    public void setUnReceivedLikeId(Long unReceivedLikeId) {
        this.unReceivedLikeId = unReceivedLikeId;
    }

    public Long getLikeId() {
        return likeId;
    }

    public void setLikeId(Long likeId) {
        this.likeId = likeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnReceivedLikeHistory that = (UnReceivedLikeHistory) o;

        if (unReceivedLikeId != null ? !unReceivedLikeId.equals(that.unReceivedLikeId) : that.unReceivedLikeId != null)
            return false;
        if (likeId != null ? !likeId.equals(that.likeId) : that.likeId != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = unReceivedLikeId != null ? unReceivedLikeId.hashCode() : 0;
        result = 31 * result + (likeId != null ? likeId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
