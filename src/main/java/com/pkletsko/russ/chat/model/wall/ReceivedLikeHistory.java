package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "received_like_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ReceivedLikeHistory implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "receivedLikeId")
    private Long receivedLikeId;

	@Column(name = "likeId")
	private Long likeId;

    @Column(name = "userId")
    private Long userId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getReceivedLikeId() {
        return receivedLikeId;
    }

    public void setReceivedLikeId(Long receivedLikeId) {
        this.receivedLikeId = receivedLikeId;
    }

    public Long getLikeId() {
        return likeId;
    }

    public void setLikeId(Long likeId) {
        this.likeId = likeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReceivedLikeHistory that = (ReceivedLikeHistory) o;

        if (receivedLikeId != null ? !receivedLikeId.equals(that.receivedLikeId) : that.receivedLikeId != null)
            return false;
        if (likeId != null ? !likeId.equals(that.likeId) : that.likeId != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = receivedLikeId != null ? receivedLikeId.hashCode() : 0;
        result = 31 * result + (likeId != null ? likeId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
