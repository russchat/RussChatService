package com.pkletsko.russ.chat.model.wall;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "wall", catalog = "russchat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Wall implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue
	@Column(name = "wallId")
	private Long wallId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfWall;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "wall_user", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "wallId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> followingUsers = new HashSet<User>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wall")
    private Set<WallMessage> wallMessages = new HashSet<WallMessage>();


    public void addUser(User user) {
        this.followingUsers.add(user);
    }

    public Set<User> getFollowingUsers() {
        return followingUsers;
    }

    public void setFollowingUsers(Set<User> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public Long getWallId() {
        return wallId;
    }

    public void setWallId(Long wallId) {
        this.wallId = wallId;
    }

    public User getOwnerOfWall() {
        return ownerOfWall;
    }

    public void setOwnerOfWall(User ownerOfWall) {
        this.ownerOfWall = ownerOfWall;
    }

    public Set<WallMessage> getWallMessages() {
        return wallMessages;
    }

    public void setWallMessages(Set<WallMessage> wallMessages) {
        this.wallMessages = wallMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wall wall = (Wall) o;

        if (wallId != null ? !wallId.equals(wall.wallId) : wall.wallId != null) return false;
        if (ownerOfWall != null ? !ownerOfWall.equals(wall.ownerOfWall) : wall.ownerOfWall != null) return false;
        if (followingUsers != null ? !followingUsers.equals(wall.followingUsers) : wall.followingUsers != null)
            return false;
        return !(wallMessages != null ? !wallMessages.equals(wall.wallMessages) : wall.wallMessages != null);

    }

    @Override
    public int hashCode() {
        int result = wallId != null ? wallId.hashCode() : 0;
//        result = 31 * result + (ownerOfWall != null ? ownerOfWall.hashCode() : 0);
//        result = 31 * result + (followingUsers != null ? followingUsers.hashCode() : 0);
        result = 31 * result + (wallMessages != null ? wallMessages.hashCode() : 0);
        return result;
    }
}
