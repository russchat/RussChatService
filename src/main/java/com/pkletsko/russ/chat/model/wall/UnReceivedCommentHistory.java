package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "unreceived_comment_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UnReceivedCommentHistory implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "unReceivedCommentId")
    private Long unReceivedCommentId;

	@Column(name = "commentId")
	private Long commentId;

    @Column(name = "userId")
    private Long userId;

    public Long getUnReceivedCommentId() {
        return unReceivedCommentId;
    }

    public void setUnReceivedCommentId(Long unReceivedCommentId) {
        this.unReceivedCommentId = unReceivedCommentId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnReceivedCommentHistory that = (UnReceivedCommentHistory) o;

        if (unReceivedCommentId != null ? !unReceivedCommentId.equals(that.unReceivedCommentId) : that.unReceivedCommentId != null)
            return false;
        if (commentId != null ? !commentId.equals(that.commentId) : that.commentId != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = unReceivedCommentId != null ? unReceivedCommentId.hashCode() : 0;
        result = 31 * result + (commentId != null ? commentId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
