package com.pkletsko.russ.chat.model.common.io;

import com.google.common.base.Optional;

import static com.pkletsko.russ.chat.model.common.io.StorageItemType.IMAGE;

public class Image extends StorageItem {

    private Optional<Integer> heigth;
    private Optional<Integer> width;

    /**
     * Default constructor to handle JSON-mapping
     *
     * NOTE: Not to be used in other "contexts"
     */
    public Image() {
        super();
    }

    public Image(String uri, String name, String contentType) {
        this(uri, name, contentType, null, null);
    }

    public Image(String uri, String name, String contentType, Integer heigth, Integer width) {
        super(uri, name, contentType);
        this.heigth = Optional.fromNullable(heigth);
        this.width = Optional.fromNullable(width);
    }

    public Optional<Integer> getHeigth() {
        return heigth;
    }

    public Optional<Integer> getWidth() {
        return width;
    }

    @Override
    public StorageItemType getStorageItemType() {
        return IMAGE;
    }
}
