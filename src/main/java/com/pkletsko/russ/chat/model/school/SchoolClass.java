package com.pkletsko.russ.chat.model.school;

import com.pkletsko.russ.chat.model.common.BaseObject;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.standard.StandardFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Indexed
@Table(name = "school_class", catalog = "russchat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@AnalyzerDef(name = "schoolclasssearchtokenanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = StandardFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = StopFilterFactory.class, params = {@Parameter(name = "ignoreCase", value = "true") }) })
@Analyzer(definition = "schoolclasssearchtokenanalyzer")
public class SchoolClass extends BaseObject implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "schoolClassId")
	private int schoolClassId;

	@Column(name = "className")
    @Field(index= Index.YES, analyze= Analyze.YES, store=Store.NO)
	private String className;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="schoolId",referencedColumnName="schoolId")
    private School school;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public int getSchoolClassId() {
        return schoolClassId;
    }

    public void setSchoolClassId(int schoolClassId) {
        this.schoolClassId = schoolClassId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SchoolClass that = (SchoolClass) o;

        if (schoolClassId != that.schoolClassId) return false;
        if (className != null ? !className.equals(that.className) : that.className != null) return false;
        return !(school != null ? !school.equals(that.school) : that.school != null);

    }

    @Override
    public int hashCode() {
        int result = schoolClassId;
        result = 31 * result + (className != null ? className.hashCode() : 0);
        result = 31 * result + (school != null ? school.hashCode() : 0);
        return result;
    }
}
