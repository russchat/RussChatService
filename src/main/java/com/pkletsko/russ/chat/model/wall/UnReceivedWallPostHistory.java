package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "unreceived_wall_post_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UnReceivedWallPostHistory implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "unReceivedWallPostId")
    private Long unReceivedWallPostId;

	@Column(name = "wallMessageId")
	private Long wallMessageId;

    @Column(name = "userId")
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUnReceivedWallPostId() {
        return unReceivedWallPostId;
    }

    public void setUnReceivedWallPostId(Long unReceivedWallPostId) {
        this.unReceivedWallPostId = unReceivedWallPostId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnReceivedWallPostHistory that = (UnReceivedWallPostHistory) o;

        if (unReceivedWallPostId != null ? !unReceivedWallPostId.equals(that.unReceivedWallPostId) : that.unReceivedWallPostId != null)
            return false;
        if (wallMessageId != null ? !wallMessageId.equals(that.wallMessageId) : that.wallMessageId != null)
            return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = unReceivedWallPostId != null ? unReceivedWallPostId.hashCode() : 0;
        result = 31 * result + (wallMessageId != null ? wallMessageId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
