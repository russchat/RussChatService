package com.pkletsko.russ.chat.model.conversation;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "received_message")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ReceivedConversationMessage implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "receivedMessageId")
    private Long receivedMessageId;

	@Column(name = "messageId")
	private Long messageId;

    @Column(name = "userId")
    private Long userId;

    public Long getReceivedMessageId() {
        return receivedMessageId;
    }

    public void setReceivedMessageId(Long receivedMessageId) {
        this.receivedMessageId = receivedMessageId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReceivedConversationMessage that = (ReceivedConversationMessage) o;

        if (receivedMessageId != null ? !receivedMessageId.equals(that.receivedMessageId) : that.receivedMessageId != null)
            return false;
        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = receivedMessageId != null ? receivedMessageId.hashCode() : 0;
        result = 31 * result + (messageId != null ? messageId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
