package com.pkletsko.russ.chat.model.wall;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "comment_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CommentHistory implements Serializable {

	private static final long serialVersionUID = 3L;

	@Id
	@GeneratedValue
	@Column(name = "commentId")
	private Long commentId;

	@Column(name = "commentText")
	private String commentText;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallMessageId", nullable = false)
    private WallMessage wallMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfComment;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "received_comment_history", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "commentId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersReceivedTheComment = new HashSet<User>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "unreceived_comment_history", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "commentId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersUnReceivedTheComment = new HashSet<User>();

    public Set<User> getUsersUnReceivedTheComment() {
        return usersUnReceivedTheComment;
    }

    public void setUsersUnReceivedTheComment(Set<User> usersUnReceivedTheComment) {
        this.usersUnReceivedTheComment = usersUnReceivedTheComment;
    }

    public Set<User> getUsersReceivedTheComment() {
        return usersReceivedTheComment;
    }

    public void addUsersReceivedTheComment(User user) {
        boolean needToAdd = true;
        for (User user1 : usersReceivedTheComment) {
            if (user1.equals(user)) {
                needToAdd = false;
            }
        }

        if (needToAdd) {
            this.usersReceivedTheComment.add(user);
        }
    }

    public void removeUsersReceivedTheComment(User user) {
        this.usersReceivedTheComment.remove(user);
    }

    public void addUsersUnReceivedTheComment(User user) {
        boolean needToAdd = true;
        for (User user1 : usersUnReceivedTheComment) {
            if (user1.equals(user)) {
                needToAdd = false;
            }
        }

        if (needToAdd) {
            this.usersUnReceivedTheComment.add(user);
        }
    }

    public void removeUsersUnReceivedTheComment(User user) {
        this.usersUnReceivedTheComment.remove(user);
    }

    public void setUsersReceivedTheComment(Set<User> usersReceivedTheComment) {
        this.usersReceivedTheComment = usersReceivedTheComment;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public WallMessage getWallMessage() {
        return wallMessage;
    }

    public void setWallMessage(WallMessage wallMessage) {
        this.wallMessage = wallMessage;
    }

    public User getOwnerOfComment() {
        return ownerOfComment;
    }

    public void setOwnerOfComment(User ownerOfComment) {
        this.ownerOfComment = ownerOfComment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentHistory that = (CommentHistory) o;

        if (commentId != null ? !commentId.equals(that.commentId) : that.commentId != null) return false;
        if (commentText != null ? !commentText.equals(that.commentText) : that.commentText != null) return false;
        if (wallMessage != null ? !wallMessage.equals(that.wallMessage) : that.wallMessage != null) return false;
        if (ownerOfComment != null ? !ownerOfComment.equals(that.ownerOfComment) : that.ownerOfComment != null)
            return false;
        if (usersReceivedTheComment != null ? !usersReceivedTheComment.equals(that.usersReceivedTheComment) : that.usersReceivedTheComment != null)
            return false;
        return !(usersUnReceivedTheComment != null ? !usersUnReceivedTheComment.equals(that.usersUnReceivedTheComment) : that.usersUnReceivedTheComment != null);

    }

    @Override
    public int hashCode() {
        int result = commentId != null ? commentId.hashCode() : 0;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
//        result = 31 * result + (wallMessage != null ? wallMessage.hashCode() : 0);
        //result = 31 * result + (ownerOfComment != null ? ownerOfComment.hashCode() : 0);
//        result = 31 * result + (usersReceivedTheComment != null ? usersReceivedTheComment.hashCode() : 0);
//        result = 31 * result + (usersUnReceivedTheComment != null ? usersUnReceivedTheComment.hashCode() : 0);
        return result;
    }
}
