package com.pkletsko.russ.chat.model.user;

import com.pkletsko.russ.chat.model.conversation.Conversation;
import com.pkletsko.russ.chat.model.conversation.ConversationMessage;
import com.pkletsko.russ.chat.model.school.SchoolClass;
import com.pkletsko.russ.chat.model.wall.*;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.standard.StandardFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Indexed
@Table(name = "user", catalog = "russchat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@AnalyzerDef(name = "searchtokenanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = StandardFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = StopFilterFactory.class, params = {@Parameter(name = "ignoreCase", value = "true") }) })
@Analyzer(definition = "searchtokenanalyzer")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "userId")
	private Long userId;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

    @Column(name = "fullName")
    @Field(index= Index.YES, analyze=Analyze.YES, store=Store.NO)
    private String fullName;

	@Column(name = "email")
	private String email;

    @Column(name = "loginId")
    private String loginId;

    @Column(name = "userTypeId")
    private int userTypeId;

    @Column(name = "profileIcon")
    private String profileIcon;

    @Column(name = "colour")
    private int colour;

    @Column(name = "nickName")
    private String nickName;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "birthDate")
    private String birthDate;

    @Column(name = "version")
    private int version;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="schoolClass", referencedColumnName="schoolClassId")
    private SchoolClass schoolClass;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private Set<Conversation> conversations = new HashSet<Conversation>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersReceivedTheMsg")
    private Set<ConversationMessage> receivedMessages = new HashSet<ConversationMessage>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfMessage")
    private Set<ConversationMessage> myMessages = new HashSet<ConversationMessage>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfWallMessage")
    private Set<WallMessage> myWallMessages = new HashSet<WallMessage>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfWall")
    private Set<Wall> myWalls = new HashSet<Wall>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfLike")
    private Set<LikeHistory> myLikes = new HashSet<LikeHistory>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfComment")
    private Set<CommentHistory> myComments = new HashSet<CommentHistory>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ownerOfAttachment")
    private Set<AttachmentHistory> myAttachments = new HashSet<AttachmentHistory>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "followingUsers")
    private Set<Wall> walls = new TreeSet<>(new WallComparator());

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersReceivedTheWallMsg")
    private Set<WallMessage> receivedWallMessages = new HashSet<WallMessage>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersReceivedTheLike")
    private Set<LikeHistory> receivedLikes = new HashSet<LikeHistory>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersUnReceivedTheLike")
    private Set<LikeHistory> unReceivedLikes = new HashSet<LikeHistory>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersReceivedTheComment")
    private Set<CommentHistory> receivedComments = new HashSet<CommentHistory>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersUnReceivedTheComment")
    private Set<CommentHistory> unReceivedComments = new HashSet<CommentHistory>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersUnReceivedTheWallPost")
    private Set<WallMessage> unReceivedWallPosts = new HashSet<WallMessage>();

    @Column(name = "created" , columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "updated" , columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    public Set<WallMessage> getUnReceivedWallPosts() {
        return unReceivedWallPosts;
    }

    public void setUnReceivedWallPosts(Set<WallMessage> unReceivedWallPosts) {
        this.unReceivedWallPosts = unReceivedWallPosts;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Set<LikeHistory> getUnReceivedLikes() {
        return unReceivedLikes;
    }

    public void setUnReceivedLikes(Set<LikeHistory> unReceivedLikes) {
        this.unReceivedLikes = unReceivedLikes;
    }

    public Set<CommentHistory> getUnReceivedComments() {
        return unReceivedComments;
    }

    public void setUnReceivedComments(Set<CommentHistory> unReceivedComments) {
        this.unReceivedComments = unReceivedComments;
    }

    public Set<LikeHistory> getReceivedLikes() {
        return receivedLikes;
    }

    public void setReceivedLikes(Set<LikeHistory> receivedLikes) {
        this.receivedLikes = receivedLikes;
    }

    public Set<CommentHistory> getReceivedComments() {
        return receivedComments;
    }

    public void setReceivedComments(Set<CommentHistory> receivedComments) {
        this.receivedComments = receivedComments;
    }

    public Set<CommentHistory> getMyComments() {
        return myComments;
    }

    public void setMyComments(Set<CommentHistory> myComments) {
        this.myComments = myComments;
    }

    public Set<LikeHistory> getMyLikes() {
        return myLikes;
    }

    public void setMyLikes(Set<LikeHistory> myLikes) {
        this.myLikes = myLikes;
    }

    public Set<WallMessage> getMyWallMessages() {
        return myWallMessages;
    }

    public void setMyWallMessages(Set<WallMessage> myWallMessages) {
        this.myWallMessages = myWallMessages;
    }

    public Set<Wall> getMyWalls() {
        return myWalls;
    }

    public void setMyWalls(Set<Wall> myWalls) {
        this.myWalls = myWalls;
    }

    public Set<Wall> getWalls() {
        return walls;
    }

    public void setWalls(Set<Wall> walls) {
        this.walls = walls;
    }

    public Set<WallMessage> getReceivedWallMessages() {
        return receivedWallMessages;
    }

    public void setReceivedWallMessages(Set<WallMessage> receivedWallMessages) {
        this.receivedWallMessages = receivedWallMessages;
    }

    public Set<ConversationMessage> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(Set<ConversationMessage> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public Set<ConversationMessage> getMyMessages() {
        return myMessages;
    }

    public void setMyMessages(Set<ConversationMessage> myMessages) {
        this.myMessages = myMessages;
    }

    public void addConversation(Conversation conversation) {
        this.conversations.add(conversation);
    }

    public Set<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(Set<Conversation> conversations) {
        this.conversations = conversations;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public Set<AttachmentHistory> getMyAttachments() {
        return myAttachments;
    }

    public void setMyAttachments(Set<AttachmentHistory> myAttachments) {
        this.myAttachments = myAttachments;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

//    @Override
//    public int compareTo(Object o) {
//        User user = (User) o;
//        Long result = this.userId - user.getUserId();
//        return result.intValue();
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (userTypeId != user.userTypeId) return false;
        if (colour != user.colour) return false;
        if (version != user.version) return false;
        if (userId != null ? !userId.equals(user.userId) : user.userId != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (fullName != null ? !fullName.equals(user.fullName) : user.fullName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (loginId != null ? !loginId.equals(user.loginId) : user.loginId != null) return false;
        if (profileIcon != null ? !profileIcon.equals(user.profileIcon) : user.profileIcon != null) return false;
        if (nickName != null ? !nickName.equals(user.nickName) : user.nickName != null) return false;
        if (country != null ? !country.equals(user.country) : user.country != null) return false;
        if (city != null ? !city.equals(user.city) : user.city != null) return false;
        if (birthDate != null ? !birthDate.equals(user.birthDate) : user.birthDate != null) return false;
        if (schoolClass != null ? !schoolClass.equals(user.schoolClass) : user.schoolClass != null) return false;
        if (conversations != null ? !conversations.equals(user.conversations) : user.conversations != null)
            return false;
        if (receivedMessages != null ? !receivedMessages.equals(user.receivedMessages) : user.receivedMessages != null)
            return false;
        if (myMessages != null ? !myMessages.equals(user.myMessages) : user.myMessages != null) return false;
        if (myWallMessages != null ? !myWallMessages.equals(user.myWallMessages) : user.myWallMessages != null)
            return false;
        if (myWalls != null ? !myWalls.equals(user.myWalls) : user.myWalls != null) return false;
        if (myLikes != null ? !myLikes.equals(user.myLikes) : user.myLikes != null) return false;
        if (myComments != null ? !myComments.equals(user.myComments) : user.myComments != null) return false;
        if (myAttachments != null ? !myAttachments.equals(user.myAttachments) : user.myAttachments != null)
            return false;
        if (walls != null ? !walls.equals(user.walls) : user.walls != null) return false;
        if (receivedWallMessages != null ? !receivedWallMessages.equals(user.receivedWallMessages) : user.receivedWallMessages != null)
            return false;
        if (receivedLikes != null ? !receivedLikes.equals(user.receivedLikes) : user.receivedLikes != null)
            return false;
        if (unReceivedLikes != null ? !unReceivedLikes.equals(user.unReceivedLikes) : user.unReceivedLikes != null)
            return false;
        if (receivedComments != null ? !receivedComments.equals(user.receivedComments) : user.receivedComments != null)
            return false;
        if (unReceivedComments != null ? !unReceivedComments.equals(user.unReceivedComments) : user.unReceivedComments != null)
            return false;
        if (unReceivedWallPosts != null ? !unReceivedWallPosts.equals(user.unReceivedWallPosts) : user.unReceivedWallPosts != null)
            return false;
        if (created != null ? !created.equals(user.created) : user.created != null) return false;
        return !(updated != null ? !updated.equals(user.updated) : user.updated != null);

    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (loginId != null ? loginId.hashCode() : 0);
        result = 31 * result + userTypeId;
        result = 31 * result + (profileIcon != null ? profileIcon.hashCode() : 0);
        result = 31 * result + colour;
        result = 31 * result + (nickName != null ? nickName.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + version;
        result = 31 * result + (schoolClass != null ? schoolClass.hashCode() : 0);
        result = 31 * result + (conversations != null ? conversations.hashCode() : 0);
        result = 31 * result + (receivedMessages != null ? receivedMessages.hashCode() : 0);
        result = 31 * result + (myMessages != null ? myMessages.hashCode() : 0);
        result = 31 * result + (myWallMessages != null ? myWallMessages.hashCode() : 0);
        result = 31 * result + (myWalls != null ? myWalls.hashCode() : 0);
        result = 31 * result + (myLikes != null ? myLikes.hashCode() : 0);
        result = 31 * result + (myComments != null ? myComments.hashCode() : 0);
        result = 31 * result + (myAttachments != null ? myAttachments.hashCode() : 0);
        result = 31 * result + (walls != null ? walls.hashCode() : 0);
        result = 31 * result + (receivedWallMessages != null ? receivedWallMessages.hashCode() : 0);
        result = 31 * result + (receivedLikes != null ? receivedLikes.hashCode() : 0);
        result = 31 * result + (unReceivedLikes != null ? unReceivedLikes.hashCode() : 0);
        result = 31 * result + (receivedComments != null ? receivedComments.hashCode() : 0);
        result = 31 * result + (unReceivedComments != null ? unReceivedComments.hashCode() : 0);
        result = 31 * result + (unReceivedWallPosts != null ? unReceivedWallPosts.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }
}
