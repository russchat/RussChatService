package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "received_comment_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ReceivedCommentHistory implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "receivedCommentId")
    private Long receivedCommentId;

	@Column(name = "commentId")
	private Long commentId;

    @Column(name = "userId")
    private Long userId;

    public Long getReceivedCommentId() {
        return receivedCommentId;
    }

    public void setReceivedCommentId(Long receivedCommentId) {
        this.receivedCommentId = receivedCommentId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReceivedCommentHistory that = (ReceivedCommentHistory) o;

        if (receivedCommentId != null ? !receivedCommentId.equals(that.receivedCommentId) : that.receivedCommentId != null)
            return false;
        if (commentId != null ? !commentId.equals(that.commentId) : that.commentId != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = receivedCommentId != null ? receivedCommentId.hashCode() : 0;
        result = 31 * result + (commentId != null ? commentId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
