package com.pkletsko.russ.chat.model.common.io;

public enum StorageItemType {
    ATTACHMENT, LOG_CONTENT, IMAGE, TEMPLATE;
}
