package com.pkletsko.russ.chat.model.user;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 25.01.15
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
public class GoogleAccess implements Serializable {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GoogleAccess that = (GoogleAccess) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
