package com.pkletsko.russ.chat.model.common.io;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

public abstract class StorageItem {

    private String uri;

    private final String name;
    private final String contentType;

    @JsonIgnore
    public abstract StorageItemType getStorageItemType();

    protected StorageItem() {
        this.uri = null;
        this.name = null;
        this.contentType = null;
    }

    protected StorageItem(String uri, String name) {
        this(uri, name, null);
    }

    protected StorageItem(String uri, String name, String contentType) {
        setUri(uri);
        this.name = name;
        this.contentType = contentType;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(uri), "Missing URI");
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public String getContentType() {
        return contentType;
    }
}
