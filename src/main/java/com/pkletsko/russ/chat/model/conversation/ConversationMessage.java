package com.pkletsko.russ.chat.model.conversation;

import com.pkletsko.russ.chat.model.user.User;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "message")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConversationMessage implements Serializable {

	private static final long serialVersionUID = 3L;

	@Id
	@GeneratedValue
	@Column(name = "messageId")
	private Long messageId;

	@Column(name = "messageText")
	private String messageText;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conversationId", nullable = false)
    private Conversation conversation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private User ownerOfMessage;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "received_message", catalog = "russchat", joinColumns = {
            @JoinColumn(name = "messageId", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "userId",
                    nullable = false, updatable = false) })
    private Set<User> usersReceivedTheMsg = new HashSet<User>();

    public void addUsersReceivedTheMsg(User user) {
        this.usersReceivedTheMsg.add(user);
    }

    public User getOwnerOfMessage() {
        return ownerOfMessage;
    }

    public void setOwnerOfMessage(User ownerOfMessage) {
        this.ownerOfMessage = ownerOfMessage;
    }

    public Set<User> getUsersReceivedTheMsg() {
        return usersReceivedTheMsg;
    }

    public void setUsersReceivedTheMsg(Set<User> usersReceivedTheMsg) {
        this.usersReceivedTheMsg = usersReceivedTheMsg;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConversationMessage that = (ConversationMessage) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;
        if (messageText != null ? !messageText.equals(that.messageText) : that.messageText != null) return false;
        if (conversation != null ? !conversation.equals(that.conversation) : that.conversation != null) return false;
        if (ownerOfMessage != null ? !ownerOfMessage.equals(that.ownerOfMessage) : that.ownerOfMessage != null)
            return false;
        return !(usersReceivedTheMsg != null ? !usersReceivedTheMsg.equals(that.usersReceivedTheMsg) : that.usersReceivedTheMsg != null);

    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (messageText != null ? messageText.hashCode() : 0);
        result = 31 * result + (conversation != null ? conversation.hashCode() : 0);
//        result = 31 * result + (ownerOfMessage != null ? ownerOfMessage.hashCode() : 0);
//        result = 31 * result + (usersReceivedTheMsg != null ? usersReceivedTheMsg.hashCode() : 0);
        return result;
    }
}
