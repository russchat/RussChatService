package com.pkletsko.russ.chat.model.wall;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "received_wall_message")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ReceivedWallMessage implements Serializable {

	private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue
    @Column(name = "receivedWallMessageId")
    private Long receivedWallMessageId;

	@Column(name = "wallMessageId")
	private Long wallMessageId;

    @Column(name = "userId")
    private Long userId;

    public Long getReceivedWallMessageId() {
        return receivedWallMessageId;
    }

    public void setReceivedWallMessageId(Long receivedWallMessageId) {
        this.receivedWallMessageId = receivedWallMessageId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReceivedWallMessage that = (ReceivedWallMessage) o;

        if (receivedWallMessageId != null ? !receivedWallMessageId.equals(that.receivedWallMessageId) : that.receivedWallMessageId != null)
            return false;
        if (wallMessageId != null ? !wallMessageId.equals(that.wallMessageId) : that.wallMessageId != null)
            return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = receivedWallMessageId != null ? receivedWallMessageId.hashCode() : 0;
        result = 31 * result + (wallMessageId != null ? wallMessageId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
