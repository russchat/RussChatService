package com.pkletsko.russ.chat.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/check")
public class CheckServerController {

    static Log log = LogFactory.getLog(CheckServerController.class.getName());

    @ResponseBody
    @RequestMapping(value = "/checkserver", method = RequestMethod.POST)
    public String isServerAlive(@RequestParam("app") String app) throws Exception {
        //log.debug(" /checkserver : app = " + app);

        return "OK";
    }
}
