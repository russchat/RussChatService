package com.pkletsko.russ.chat.controller;

import com.google.common.base.Strings;
import com.pkletsko.russ.chat.controller.io.FileStorage;
import com.pkletsko.russ.chat.controller.io.ImageManager;
import com.pkletsko.russ.chat.model.common.io.Image;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.service.wall.AttachmentHistoryService;
import com.pkletsko.russ.chat.service.wall.CommentHistoryService;
import com.pkletsko.russ.chat.service.wall.LikeHistoryService;
import com.pkletsko.russ.chat.service.wall.WallMessageService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

import static com.pkletsko.russ.chat.controller.io.FileStorage.SUBDIRECTORY_IMAGES;
import static org.springframework.http.ResponseEntity.ok;

@Controller
@RequestMapping("/wall")
public class WallController {

    static Log log = LogFactory.getLog(WallController.class.getName());

    @Autowired
    protected Global global;

    @Autowired
    protected UserService userService;

    @Autowired
    protected WallMessageService wallMessageService;

    @Autowired
    protected LikeHistoryService likeHistoryService;

    @Autowired
    protected CommentHistoryService commentHistoryService;

    @Autowired
    protected AttachmentHistoryService attachmentHistoryService;

    private ImageManager clientImageManager;

    private final int numberOfItemsPerPage = 5;

    @PostConstruct
    private void postConstruct() {
        clientImageManager = new ImageManager(new FileStorage(global.getStorageRootDirectory(), SUBDIRECTORY_IMAGES, "post"));
    }

    @ResponseBody
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public WallPosts getUserPosts(@RequestParam("token") String token, @RequestParam("id") String userId, @RequestParam("userId") String loggedUserId, @RequestParam("postId") String lastLoadedPostId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /posts :" + token + " loggedUserId = " + loggedUserId + ", userId = " + userId + ", lastLoadedPostId = " + lastLoadedPostId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return wallMessageService.getUserPosts(Long.valueOf(userId), Long.valueOf(loggedUserId), Long.valueOf(lastLoadedPostId), numberOfItemsPerPage, Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/receivedposts", method = RequestMethod.POST)
    public WallPosts getReceivedUserPosts(@RequestParam("token") String token, @RequestParam("userId") String loggedUserId, @RequestParam("postId") String lastLoadedPostId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /receivedposts :" + token + " loggedUserId = " + loggedUserId + ", lastLoadedPostId = " + lastLoadedPostId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return wallMessageService.getReceivedUserPosts(Long.valueOf(loggedUserId), Long.valueOf(lastLoadedPostId), numberOfItemsPerPage, Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/likes", method = RequestMethod.POST)
    public Likes getUserLikes(@RequestParam("token") String token, @RequestParam("userId") String userId, @RequestParam("id") String wallPostId, @RequestParam("likeId") String lastLoadedUserLikeId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /likes :" + token + " userId = " + userId + ", wallPostId = " + wallPostId + ", lastLoadedUserLikeId = " + lastLoadedUserLikeId + ", direction = " + direction);

        if (lastLoadedUserLikeId == null) {
            Likes likes = new Likes();
            return likes;
        }

        if (global.isAccessApproved(token, Long.valueOf(userId))) {
            return likeHistoryService.getLikes(Long.valueOf(wallPostId), Long.valueOf(lastLoadedUserLikeId), numberOfItemsPerPage, Long.valueOf(userId), Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.POST)
    public Comments getComments(@RequestParam("token") String token, @RequestParam("userId") String userId, @RequestParam("id") String wallPostId, @RequestParam("commentId") String lastLoadedCommentId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /comments :" + token + " userId = " + userId + ", wallPostId = " + wallPostId + ", lastLoadedCommentId = " + lastLoadedCommentId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(userId))) {
            return commentHistoryService.getComments(Long.valueOf(wallPostId), Long.valueOf(lastLoadedCommentId), numberOfItemsPerPage, Long.valueOf(userId), Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Uploaded handleFileUpload(
            @RequestParam("file") MultipartFile file,
            @RequestParam("fromUserId") String fromUserId,
            @RequestParam("wallPostId") String wallPostId,
            @RequestParam("token") String token,
            @RequestParam("id") String userId,
            @RequestParam("attachmentId") String attachmentId) throws Exception {

        Uploaded uploaded = new Uploaded();
        uploaded.setAttachmentId(Long.valueOf(attachmentId));
        uploaded.setStatus("Error");

        String fileUserPath = fromUserId + "/" + wallPostId;

        log.debug(" /upload :" + token + " userId = " + userId + ", fileUserPath = " + fileUserPath);

        if (global.isAccessApproved(token, Long.valueOf(userId))) {
            Image image = clientImageManager.save(file.getInputStream(),
                    file.getOriginalFilename(),
                    file.getContentType(),
                    Strings.nullToEmpty(fileUserPath));

            Attachment attachment = new Attachment();
            attachment.setAttachmentName(image.getName());
            attachment.setFromUserId(Long.valueOf(fromUserId));
            attachment.setWallMessageId(Long.valueOf(wallPostId));

            //save links to database and connect to wall post
            attachmentHistoryService.saveAttachmentHistory(Long.valueOf(userId), attachment);

            uploaded.setStatus("Ok");

            if (global.updateUnCompletedWallPostMap(attachment.getWallMessageId(), attachment.getAttachmentName())) {
                uploaded.setStatus("Completed");
            }
        }

        return uploaded;
    }

    @ResponseBody
    @RequestMapping(value = "/image", method = RequestMethod.GET, produces = "image/jpeg")
    public byte[] getFile(@RequestHeader("p4") String p4, @RequestParam String p0, @RequestParam String p1, @RequestParam String p2, @RequestParam String p3) {

        //log.debug("getFile GET /image LoggedUserId: " + p0 + " , user = " + p1 + ", post = " + p2 + ", image = " + p3 + ", token = " + p4);

        if (global.isAccessApproved(p4, Long.valueOf(p0))) {
            try {

                //log.debug("Access Approved for getLoggedUserId: " + p0 + " ,  user = " + p1 + ", post = " + p2 + ", image = " + p3);

                File file = new File(global.getStorageRootDirectory() + "/images/post/" + p1 + "/" + p2 + "/" + p3 + ".jpg");
                InputStream is = new FileInputStream(file);

                // Retrieve image from the classpath.
                //InputStream is = this.getClass().getResourceAsStream("/test.jpg");

                // Prepare buffered image.
                BufferedImage bufferedImage = ImageIO.read(is);

                // Create a byte array output stream.
                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                // Write to output stream
                ImageIO.write(bufferedImage, "jpg", bao);

                byte[] result = bao.toByteArray();

                //log.debug("Byte Array of  user = " + p1 + ", post = " + p2 + ", image = " + p3 + ", Array Size = [" + result.length + "]");

                return result;
            } catch (IOException e) {
                log.error(e);
                //throw new RuntimeException(e);
                return new byte[0];
            }
        } else {
            log.debug("Access DENIED for get LoggedUserId: " + p0 + " ,  user = " + p1 + ", post = " + p2 + ", image = " + p3 + ", token = " + p4);
            return new byte[0];
        }
    }
}
