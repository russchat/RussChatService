package com.pkletsko.russ.chat.controller.io;

import org.apache.commons.lang.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

public abstract class StorageItemManager {

    private final Storage storage;

    protected StorageItemManager(@NotNull final Storage storage) {
        this.storage = storage;
    }

    protected Storage getStorage() {
        return storage;
    }

    protected String getRandomStorageName() {
        return UUID.randomUUID().toString();
    }

    protected String getRandomStorageNameWithExtension(final String extension) {
        return getRandomStorageName() + "." + StringUtils.defaultString(extension, "ext");
    }

    protected URI getStorageURI(final String id, final boolean addDateHierarchy) throws IOException {
        return storage.createURI(id, addDateHierarchy);
    }
}
