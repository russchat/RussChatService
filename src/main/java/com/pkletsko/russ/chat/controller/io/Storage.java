package com.pkletsko.russ.chat.controller.io;

import javax.activation.DataHandler;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public interface Storage {

    /**
     * Initializes the storage, i.e. create a base-URI.
     *
     * It should not be called unless the save-/process-method(s) will be called later.
     *
     * @param id TODO: Description
     * @param addDateHierarchy TODO: Description
     * @return A URI TODO: Description
     * @throws java.io.IOException
     */
    public URI createURI(String id, boolean addDateHierarchy) throws IOException;

    public URI save(DataHandler data, int counter, URI baseURI) throws IOException;

    public URI save(InputStream inputStream, String name) throws IOException;

    public URI save(InputStream inputStream, String name, URI baseURI) throws IOException;

}
