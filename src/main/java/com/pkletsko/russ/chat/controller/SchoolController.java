package com.pkletsko.russ.chat.controller;

import com.pkletsko.russ.chat.service.school.SchoolService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.user.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/school")
public class SchoolController {

    static Log log = LogFactory.getLog(SchoolController.class.getName());

    @Autowired
    protected Global global;

    @Autowired
    protected SchoolService schoolService;

    @ResponseBody
    @RequestMapping(value = "/school", method = RequestMethod.POST)
    public SchoolInfoSearchResult getSchool(@RequestParam("token") String token, @RequestParam("search") String search, @RequestParam("userId") String loggedUserId) throws Exception {
        log.debug(" /school :" + token + " loggedUserId = " + loggedUserId + ", search = " + search);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            SchoolInfoSearchResult result = new SchoolInfoSearchResult();
            result.setSchools(schoolService.searchSchool(search));
            return result;
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/schoolclass", method = RequestMethod.POST)
    public SchoolClassInfoSearchResult getSchoolClass(@RequestParam("token") String token, @RequestParam("schoolId") String schoolId, @RequestParam("search") String search, @RequestParam("userId") String loggedUserId) throws Exception {
        log.debug(" /school :" + token + " loggedUserId = " + loggedUserId + ", schoolId = " + schoolId + ", search = " + search);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            SchoolClassInfoSearchResult result = new SchoolClassInfoSearchResult();
            result.setSchoolClassInfos(schoolService.searchSchoolClass(Integer.valueOf(schoolId), search));
            return result;
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/schoolinfo", method = RequestMethod.POST)
    public SchoolInfo getSchoolInfo(@RequestParam("token") String token, @RequestParam("schoolId") String schoolId, @RequestParam("userId") String loggedUserId) throws Exception {
        log.debug(" /schoolinfo :" + token + " loggedUserId = " + loggedUserId + ", schoolId = " + schoolId);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return schoolService.getSchoolInfoById(Integer.valueOf(schoolId));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/schoolclassinfo", method = RequestMethod.POST)
    public SchoolClassInfo getSchoolClassInfo(@RequestParam("token") String token, @RequestParam("schoolClassId") String schoolClassId, @RequestParam("userId") String loggedUserId) throws Exception {
        log.debug(" /schoolclassinfo :" + token + " loggedUserId = " + loggedUserId + ", schoolClassId = " + schoolClassId);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return schoolService.getSchoolClassInfoById(Integer.valueOf(schoolClassId));
        }

        return null;
    }
}
