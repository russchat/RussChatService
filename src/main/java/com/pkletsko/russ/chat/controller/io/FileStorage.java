package com.pkletsko.russ.chat.controller.io;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.activation.DataHandler;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

/**
 * File specific storage
 *
 */
public class FileStorage implements Storage {

    private static final Log LOGGER = LogFactory.getLog(FileStorage.class.getName());

    private static final String UNKNOWN_FILENAME = "unknown_filename";

    public final static String SUBDIRECTORY_ATTACHMENTS = "attachments";

    public final static String SUBDIRECTORY_DOCUMENTS = "documents";

    public final static String SUBDIRECTORY_IMAGES = "images";

    public final static String SUBDIRECTORY_TEMPLATES = "templates";

    public final static String SUBDIRECTORY_LETTER_OF_GUARANTEE = "letter-of-guarantee";

    public final static String SUBDIRECTORY_TMP = "tmp";

    private File storage;

    private FileStorage(File file) {
        Preconditions.checkNotNull(file, "Missing file");
        this.storage = createDirectoryIfNonExisting(file);
    }

    public FileStorage(@NotNull final String rootDirectory) {
        this(Paths.get(rootDirectory).toFile());
    }

    public FileStorage(@NotNull final String rootDirectory, @NotNull final String... subDirectories) {
        this(Paths.get(rootDirectory, subDirectories).toFile());
    }

    @Override
    public URI save(DataHandler data, int counter, URI baseURI) throws IOException {
        Preconditions.checkNotNull(baseURI, "Missing URI");

        File baseDirectory = new File(baseURI);

        checkState(baseDirectory.exists(), "URI doesn't exist: " + baseURI.getPath());

        String name = defaultIfEmpty(data.getName(), UNKNOWN_FILENAME);
        String fileName = encodeFilename(name, counter);
        File outputFile = new File(baseDirectory, fileName);

        try (FileOutputStream fos = new FileOutputStream(outputFile)) {
            data.writeTo(fos);
        }
        return outputFile.toURI();
    }

    @Override
    public URI save(InputStream inputStream, String fileName) throws IOException {
        return save(inputStream, fileName, null);
    }

    @Override
    public URI save(InputStream inputStream, String fileName, URI baseURI) throws IOException {
        Path targetPath = resolvePath(baseURI != null ? baseURI : storage.toURI(), encodeFilename(fileName));
        //Files.copy(inputStream, targetPath, StandardCopyOption.REPLACE_EXISTING);

        File targetFile = targetPath.toFile();

        FileUtils.copyInputStreamToFile(inputStream, targetFile);

        return targetPath.toUri();
    }

    /**
     * Generates a file used for storing files in a sub-directory - with or without date-sub-directories
     *
     * @param subDirectory
     * @param addDateHierarchy
     * @return Sub-directory (File)
     */
    public File getStorageDirectory(@NotNull final String subDirectory,
                                    final boolean addDateHierarchy,
                                    final boolean createIfNotExists) {
        File base = storage;

        if (addDateHierarchy) {
            DateTime now = DateTime.now();
            DateTimeFormatter format = DateTimeFormat.forPattern("yyyy/MM/dd");
            base = new File(storage, format.print(now));
        }

        if (createIfNotExists) {
            return createDirectoryIfNonExisting(new File(base, subDirectory));
        } else {
            return new File(base, subDirectory);
        }
    }

    @Override
    public URI createURI(String id, boolean addDateHierarchy) throws IOException {
        return getStorageDirectory(id, addDateHierarchy, true).toURI();
    }

    private File createDirectoryIfNonExisting(File file) {
        if (!file.exists() && !file.mkdirs()) {
            LOGGER.error("Failed to create " + file.getAbsolutePath());
        }
        return file;
    }

    private Path resolvePath(final URI uri) {
        Path path = Paths.get(uri);
        Preconditions.checkArgument(Files.exists(path), "File doesn't exist: " + uri.getPath());
        return path;
    }

    private Path resolvePath(final URI uri, final String fileName) {
        return Paths.get(new File(new File(uri), fileName).toURI());
    }

    private String encodeFilename(String fileName) throws IOException {
        return encodeFilename(fileName, 0);
    }

    private String encodeFilename(String fileName, final int counter) throws IOException {
        String urlEncodedFileName = URLEncoder.encode(fileName, "UTF-8");
        if (counter > 0) {
            return String.format("%s_%s", counter, urlEncodedFileName);
        } else {
            return urlEncodedFileName;
        }
    }
}
