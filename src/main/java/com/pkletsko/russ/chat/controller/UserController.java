package com.pkletsko.russ.chat.controller;

import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.user.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    static Log log = LogFactory.getLog(UserController.class.getName());

    @Autowired
    protected Global global;

    @Autowired
    protected UserService userService;

    private final int numberOfItemsPerPage = 5;

    @ResponseBody
    @RequestMapping(value = "/followers", method = RequestMethod.POST)
    public UserProfiles getFollowers(@RequestParam("token") String token, @RequestParam("id") String userId, @RequestParam("userId") String loggedUserId, @RequestParam("lastUserId") String lastUserId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /followers :" + token + " userId = " + userId + ", loggedUserId = " + loggedUserId + ", lastUserId = " + lastUserId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return userService.getFollowers(Long.valueOf(loggedUserId), Long.valueOf(userId), Long.valueOf(lastUserId), numberOfItemsPerPage, Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/following", method = RequestMethod.POST)
    public UserProfiles getFollowing(@RequestParam("token") String token, @RequestParam("id") String userId, @RequestParam("userId") String loggedUserId, @RequestParam("lastUserId") String lastUserId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /following :" + token + " userId = " + userId + ", loggedUserId = " + loggedUserId + ", lastUserId = " + lastUserId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(loggedUserId))) {
            return userService.getFollowing(Long.valueOf(loggedUserId), Long.valueOf(userId), Long.valueOf(lastUserId), numberOfItemsPerPage, Integer.valueOf(direction));
        }

        return null;
    }
}
