package com.pkletsko.russ.chat.controller.io;

import com.pkletsko.russ.chat.model.common.io.Image;
import javax.validation.constraints.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class ImageManager extends StorageItemManager {

    public ImageManager(@NotNull final Storage storage) {
        super(storage);
    }

    public Image save(@NotNull InputStream inputStream,
                      @NotNull String name,
                      @NotNull String contentType,
                      @NotNull String uriSubPath) throws IOException {
//        String storageName = getRandomStorageName();
//
//        contentType = StringUtils.defaultString(contentType, "").toLowerCase();
//
//        if (StringUtils.startsWith(contentType, "image/")) {
//            storageName = getRandomStorageNameWithExtension(StringUtils.substringAfter(contentType, "image/"));
//        }

        URI uri = getStorage().save(inputStream, name, getStorageURI(uriSubPath, false));

        BufferedImage image = ImageIO.read(new File(uri));

        int heigth = image.getHeight();
        int width = image.getWidth();

        return new Image(uri.toString(), name, contentType, heigth, width);
    }
}
