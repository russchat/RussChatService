package com.pkletsko.russ.chat.controller;

import com.pkletsko.russ.chat.service.conversation.ConversationMessageService;
import com.pkletsko.russ.chat.service.conversation.ConversationService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Conversations;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/chat")
public class ChatController {

    static Log log = LogFactory.getLog(ChatController.class.getName());

    @Autowired
    protected Global global;

    @Autowired
    protected ConversationService conversationService;

    @Autowired
    protected ConversationMessageService conversationMessageService;

    private final int numberOfItemsPerPage = 5;

    @ResponseBody
    @RequestMapping(value = "/chatmessages", method = RequestMethod.POST)
    public ChatMessages getConversationMessages(@RequestParam("token") String token, @RequestParam("userId") String userId, @RequestParam("id") String conversationId, @RequestParam("chatMessageId") String lastLoadedMsgId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /chatmessages :" + token + " userId = " + userId + ", conversationId = " + conversationId + ", lastLoadedMsgId = " + lastLoadedMsgId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(userId))) {
            return conversationMessageService.getConversationMessages(Long.valueOf(conversationId), Long.valueOf(lastLoadedMsgId), numberOfItemsPerPage, Long.valueOf(userId), Integer.valueOf(direction));
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/conversations", method = RequestMethod.POST)
    public Conversations getConversations(@RequestParam("token") String token, @RequestParam("userId") String userId, @RequestParam("conversationId") String lastLoadedConversationId, @RequestParam("direction") String direction) throws Exception {
        log.debug(" /conversations :" + token + " userId = " + userId + ", lastLoadedCommentId = " + lastLoadedConversationId + ", direction = " + direction);

        if (global.isAccessApproved(token, Long.valueOf(userId))) {
            return conversationService.getConversations(Long.valueOf(userId), Long.valueOf(lastLoadedConversationId), numberOfItemsPerPage, Integer.valueOf(direction));
        }

        return null;
    }
}
