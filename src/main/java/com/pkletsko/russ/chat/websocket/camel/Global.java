package com.pkletsko.russ.chat.websocket.camel;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.PostponedWallPost;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pkletsko on 26.11.2015.
 */
@Component
public class Global {
    static Log log = LogFactory.getLog(Global.class.getName());

    private String storageRootDirectory;

    // Mapping between session and person name
    private Map<String, User> userWebsocketSessionKeyPair = new ConcurrentHashMap<String, User>();

    private Map<Long, String> userSessionKeyPair = new ConcurrentHashMap<Long, String>();

    private Map<Long, String> userIdConnectionKeyPair = new ConcurrentHashMap<Long, String>();

    private Map<Long, PostponedWallPost> unCompletedWallPostMap = new ConcurrentHashMap<>();


    public Map<String, User> getUserWebsocketSessionKeyPair() {
        return userWebsocketSessionKeyPair;
    }

    public Map<Long, String> getUserSessionKeyPair() {
        return userSessionKeyPair;
    }

    public Map<Long, PostponedWallPost> getUnCompletedWallPostMap() {
        return unCompletedWallPostMap;
    }

    public void addUserWebsocketSessionKeyPair(final String websocketSessionKey, final User user) {
        userWebsocketSessionKeyPair.put(websocketSessionKey, user);
    }

    public void addUserSessionKeyPair(final Long userId, final String token) {
        userSessionKeyPair.put(userId, token);
    }

    public void removeUserWebsocketSessionKeyPair(final String websocketSessionKey) {
        userWebsocketSessionKeyPair.remove(websocketSessionKey);
    }

    public void removeUserSessionKeyPair(final Long userId) {
        userSessionKeyPair.remove(userId);
    }

    public void removeUserIdConnectionKeyPair(final Long userId) {
        userIdConnectionKeyPair.remove(userId);
    }

    public User getUserByWebsocketSessionKey(final String websocketSessionKey) {
        return userWebsocketSessionKeyPair.get(websocketSessionKey);
    }

    public String getTokenByUserId(final Long userId) {
        return userSessionKeyPair.get(userId);
    }

    public Map<Long, String> getUserIdConnectionKeyPair() {
        return userIdConnectionKeyPair;
    }

    public void setUserIdConnectionKeyPair(Map<Long, String> userIdConnectionKeyPair) {
        this.userIdConnectionKeyPair = userIdConnectionKeyPair;
    }

    public String getStorageRootDirectory() {
        return storageRootDirectory;
    }

    public void setStorageRootDirectory(String storageRootDirectory) {
        this.storageRootDirectory = storageRootDirectory;
    }

    public void addUserIdConnectionKeyPair(final Long userId, final String connectionKey) {
        userIdConnectionKeyPair.put(userId, connectionKey);
    }

    public User getSessionUser(final String connectionKey) {
        if (connectionKey == null) {
            return null;
        }
        return getUserWebsocketSessionKeyPair().get(connectionKey);
    }

    public boolean updateUnCompletedWallPostMap(final Long wallPostId, final String attachmentPath) {
        if (unCompletedWallPostMap.containsKey(wallPostId)) {
            if (unCompletedWallPostMap.get(wallPostId).addLoadedAttachment(attachmentPath)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAccessApproved(final String token, final Long userId) {
        final String existedToken = getTokenByUserId(userId);

        //log.debug("isAccessApproved token = " + token + ", existedToken = " + existedToken + ", userId = " + userId);

        if (existedToken != null && existedToken.equals(token)) {
            //log.debug("OK isAccessApproved token = " + token + ", existedToken = " + existedToken + ", userId = " + userId);
            return true;
        }

        log.debug("FAIL isAccessApproved token = " + token + ", existedToken = " + existedToken + ", userId = " + userId);
        return false;
    }

}