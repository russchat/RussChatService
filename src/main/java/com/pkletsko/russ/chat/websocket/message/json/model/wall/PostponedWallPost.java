package com.pkletsko.russ.chat.websocket.message.json.model.wall;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkletsko on 31.08.2015.
 */
public class PostponedWallPost {
    private WallPost wallPost;
    private Set<String> loadedAttachments  = new HashSet<>();

    public boolean addLoadedAttachment(final String attachment) {
        loadedAttachments.add(attachment);
        return isWallPostCompleted();
    }

    public boolean isWallPostCompleted() {
         if (wallPost.getAttachments().size() == loadedAttachments.size()) {
             return true;
         }
        return false;
    }

    public WallPost getWallPost() {
        return wallPost;
    }

    public void setWallPost(WallPost wallPost) {
        this.wallPost = wallPost;
    }

    public Set<String> getLoadedAttachments() {
        return loadedAttachments;
    }

    public void setLoadedAttachments(Set<String> loadedAttachments) {
        this.loadedAttachments = loadedAttachments;
    }
}
