package com.pkletsko.russ.chat.websocket.message.json.model.wall;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class LikeComp implements Comparator<Like> {
    @Override
    public int compare(Like e1, Like e2) {
        final long id1 = e1.getWallMessageId(), id2 = e2.getWallMessageId();
        if (id1 == id2)
            return 0;
        return id1 < id2 ? 1 : -1;
    }
}
