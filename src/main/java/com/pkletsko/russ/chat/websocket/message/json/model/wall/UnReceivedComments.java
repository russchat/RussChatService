package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class UnReceivedComments extends BasicTransportJSONObject implements JSONResponse {

    private Set<Comment> comments;

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
