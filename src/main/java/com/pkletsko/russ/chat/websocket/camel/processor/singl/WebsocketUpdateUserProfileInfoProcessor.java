/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pkletsko.russ.chat.websocket.camel.processor.singl;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.atmosphere.websocket.WebsocketConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil.logging;
import static com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil.sendWS;

public class WebsocketUpdateUserProfileInfoProcessor implements Processor {
    private static final Logger log = LoggerFactory.getLogger(WebsocketUpdateUserProfileInfoProcessor.class);

    @Autowired
    private Global global;

    @Autowired
    private UserService userServices;

    public void process(Exchange exchange) throws Exception {
        String clientMessage = exchange.getIn().getBody(String.class);
        String key = exchange.getIn().getHeader(WebsocketConstants.CONNECTION_KEY, String.class);
        final User currentUser = global.getSessionUser(key);

        // User is not online
        if (currentUser == null) {
            throw new Exception();
        }

        String message = userServices.getUserProfileUpdate(clientMessage, currentUser);
        logging(exchange, message, log);
        sendWS(exchange, message);
    }
}
