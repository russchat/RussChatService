/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pkletsko.russ.chat.websocket.camel.processor.common;

import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.pkletsko.russ.chat.websocket.camel.processor.util.CamelConstants.MESSAGE_TYPE;

public class WebsocketMessageProcessor implements Processor {
    private static final Logger log = LoggerFactory.getLogger(WebsocketMessageProcessor.class);

    public void process(Exchange exchange) throws Exception {
        String clientMessage = exchange.getIn().getBody(String.class);
        // Parsing the json and getting message
        JSONObject jObj = new JSONObject(clientMessage);
        int objectType = jObj.getInt("objectType");
        exchange.getIn().setHeader(MESSAGE_TYPE, objectType);
    }
}
