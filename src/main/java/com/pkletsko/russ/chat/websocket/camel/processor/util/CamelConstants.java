package com.pkletsko.russ.chat.websocket.camel.processor.util;

/**
 * Created by pkletsko on 10.12.2015.
 */
public class CamelConstants {
    public static final String MESSAGE_TYPE = "websocket.message.type";
    public static final String MESSAGE_DB_ID = "stored.db.message.id";
}
