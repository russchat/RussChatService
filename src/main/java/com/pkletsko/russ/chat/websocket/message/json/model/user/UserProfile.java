package com.pkletsko.russ.chat.websocket.message.json.model.user;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 15:07
 * To change this template use File | Settings | File Templates.
 */
public class UserProfile {

    private Long userId;

    private Long userMDBId;

    private String firstName;

    private String lastName;

    private String fullName;

    private String nickName;

    private String country;

    private String city;

    private int school;

    private int schoolClass;

    private int colour;

    private String birthDate;

    private String profileIcon;

    private Long myWallId;

    private int followingWalls;

    private int followers;

    private int myPosts;

    private Boolean followed;

    private List<Long> followedWalls;

    private int version;

    private int loggedInUser;

    public List<Long> getFollowedWalls() {
        return followedWalls;
    }

    public void setFollowedWalls(List<Long> followedWalls) {
        this.followedWalls = followedWalls;
    }

    public Boolean isFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public int getFollowingWalls() {
        return followingWalls;
    }

    public void setFollowingWalls(int followingWalls) {
        this.followingWalls = followingWalls;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getMyPosts() {
        return myPosts;
    }

    public void setMyPosts(int myPosts) {
        this.myPosts = myPosts;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public Long getUserMDBId() {
        return userMDBId;
    }

    public void setUserMDBId(Long userMDBId) {
        this.userMDBId = userMDBId;
    }

    public Long getMyWallId() {
        return myWallId;
    }

    public void setMyWallId(Long myWallId) {
        this.myWallId = myWallId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getSchool() {
        return school;
    }

    public void setSchool(int school) {
        this.school = school;
    }

    public int getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(int schoolClass) {
        this.schoolClass = schoolClass;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(int loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    @Override
    public String toString() {
        return getFirstName();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UserProfile) {
            UserProfile pp = (UserProfile) o;
            return (pp.firstName.equals(this.firstName) && pp.userId == this.userId);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = userId != null ? (int)(userId.longValue()^(userId.longValue()>>>32))  : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        return result;
    }
}
