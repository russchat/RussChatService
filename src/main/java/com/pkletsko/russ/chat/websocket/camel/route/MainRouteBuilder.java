package com.pkletsko.russ.chat.websocket.camel.route;

import com.pkletsko.russ.chat.websocket.camel.processor.common.WebsocketMessageProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.common.WebsocketNotDeliveredMessageProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.common.WebsocketSessionProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.multi.*;
import com.pkletsko.russ.chat.websocket.camel.processor.none.WebsocketCreateNewSchoolClassProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.none.WebsocketCreateNewSchoolProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.none.WebsocketFollowWallProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.none.WebsocketUnFollowWallProcessor;
import com.pkletsko.russ.chat.websocket.camel.processor.singl.*;
import com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.atmosphere.websocket.WebsocketConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.pkletsko.russ.chat.websocket.camel.processor.util.CamelConstants.MESSAGE_TYPE;
import static com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil.RECIPIENTS_SET;

@Component
public class MainRouteBuilder extends RouteBuilder {

    @Autowired
    private WebsocketSessionProcessor websocketSessionProcessor;

    @Autowired
    private WebsocketMessageProcessor websocketMessageProcessor;

    @Autowired
    private WebsocketNotDeliveredMessageProcessor websocketNotDeliveredMessageProcessor;

    @Autowired
    private WebsocketUnreceivedInformationProcessor websocketUnreceivedInformationProcessor;

    @Autowired
    private WebsocketConversationProcessor websocketConversationProcessor;

    @Autowired
    private WebsocketReNewConversationProcessor websocketReNewConversationProcessor;

    @Autowired
    private WebsocketUpdateUserProfileInfoProcessor websocketUpdateUserProfileInfoProcessor;

    @Autowired
    private WebsocketSearchProcessor websocketSearchProcessor;

    @Autowired
    private WebsocketChatUserSearchProcessor websocketChatUserSearchProcessor;

    @Autowired
    private WebsocketGetUserProfileProcessor websocketGetUserProfileProcessor;

    @Autowired
    private WebsocketWallPostProcessor websocketWallPostProcessor;

    @Autowired
    private WebsocketCommentProcessor websocketCommentProcessor;

    @Autowired
    private WebsocketLikeProcessor websocketLikeProcessor;

    @Autowired
    private WebsocketChatMessageProcessor websocketChatMessageProcessor;

    @Autowired
    private WebsocketCreateNewSchoolProcessor websocketCreateNewSchoolProcessor;

    @Autowired
    private WebsocketCreateNewSchoolClassProcessor websocketCreateNewSchoolClassProcessor;

    @Autowired
    private WebsocketFollowWallProcessor websocketFollowWallProcessor;

    @Autowired
    private WebsocketUnFollowWallProcessor websocketUnFollowWallProcessor;

    @Autowired
    private WebsocketPreSendProcessor websocketPreSendProcessor;


    public void configure() {
        // Define routing rules here:
        from("atmosphere-websocket:///chat")
                .choice()
                    .when(header(WebsocketConstants.EVENT_TYPE).isEqualTo(WebsocketConstants.ONOPEN_EVENT_TYPE))
                        .process(websocketSessionProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(WebsocketConstants.EVENT_TYPE).isEqualTo(WebsocketConstants.ONCLOSE_EVENT_TYPE))
                        .process(websocketSessionProcessor)
                    .when(header(WebsocketConstants.EVENT_TYPE).isEqualTo(WebsocketConstants.ONERROR_EVENT_TYPE))
                        .process(websocketSessionProcessor)
                    .when(header(WebsocketConstants.ERROR_TYPE).isEqualTo(WebsocketConstants.MESSAGE_NOT_SENT_ERROR_TYPE))
                        .process(websocketNotDeliveredMessageProcessor)
                    .otherwise()
                        .process(websocketMessageProcessor).to("seda:messages");

        from("seda:messages?concurrentConsumers=5")
                .choice()
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_GET_UNRECEIVED_INFORMATION.getCode()))
                        .process(websocketUnreceivedInformationProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_RE_NEW_CONVERSATIONS.getCode()))
                        .process(websocketReNewConversationProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_UPDATE_USER_PROFILE_INFO.getCode()))
                        .process(websocketUpdateUserProfileInfoProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_SEARCH.getCode()))
                        .process(websocketSearchProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_CHAT_USER_SEARCH.getCode()))
                        .process(websocketChatUserSearchProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_GET_USER_PROFILE.getCode()))
                        .process(websocketGetUserProfileProcessor)
                        .to("atmosphere-websocket:///chat")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_CREATE_NEW_CONVERSATIONS.getCode()))
                        .process(websocketConversationProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_NEW_WALL_MESSAGE.getCode()))
                        .process(websocketWallPostProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_SEND_COMPLETED_WALL_POST.getCode()))
                        .process(websocketWallPostProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_COMMENT.getCode()))
                        .process(websocketCommentProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_LIKE.getCode()))
                        .process(websocketLikeProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_DISLIKE.getCode()))
                        .process(websocketLikeProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_NEW_MESSAGE.getCode()))
                        .process(websocketChatMessageProcessor)
                        .to("seda:presend")
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_CREATE_NEW_SCHOOL.getCode()))
                        .process(websocketCreateNewSchoolProcessor)
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_CREATE_NEW_SCHOOL_CLASS.getCode()))
                        .process(websocketCreateNewSchoolClassProcessor)
                    .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_FOLLOW_WALL.getCode()))
                        .process(websocketFollowWallProcessor)
                .when(header(MESSAGE_TYPE).isEqualTo(MessageObjectType.MOT_UNFOLLOW_WALL.getCode()))
                .process(websocketUnFollowWallProcessor);

        from("seda:presend?concurrentConsumers=5")
                .process(websocketPreSendProcessor)
                .to("atmosphere-websocket:///chat");
    }
}