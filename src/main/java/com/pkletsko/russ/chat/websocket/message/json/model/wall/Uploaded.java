package com.pkletsko.russ.chat.websocket.message.json.model.wall;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Uploaded extends BasicTransportJSONObject implements JSONResponse {

    private Long attachmentId;
    private String status;

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
