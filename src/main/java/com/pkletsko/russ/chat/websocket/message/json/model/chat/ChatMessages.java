package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;

public class ChatMessages extends BasicTransportJSONObject implements JSONResponse {

    private Set<ChatMessage> chatMessages = new HashSet<>();

    private Set<UserProfile> userProfiles = new HashSet<>();

    public Set<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(Set<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }
}
