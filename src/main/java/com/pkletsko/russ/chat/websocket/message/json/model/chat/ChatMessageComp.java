package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class ChatMessageComp implements Comparator<ChatMessage> {
    @Override
    public int compare(ChatMessage e1, ChatMessage e2) {
        if(e1.getMessageId() > e2.getMessageId()){
            return 1;
        } else {
            return -1;
        }
    }
}
