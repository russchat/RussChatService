package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Conversations extends BasicTransportJSONObject implements JSONResponse {

    private Set<Chat> conversations = new TreeSet<>(new ConversationComp());

    private Set<UserProfile> userProfiles = new HashSet<>();

    public Set<Chat> getConversations() {
        return conversations;
    }

    public void setConversations(Set<Chat> conversations) {
        this.conversations = conversations;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }
}
