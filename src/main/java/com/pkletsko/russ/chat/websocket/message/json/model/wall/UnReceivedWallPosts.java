package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class UnReceivedWallPosts extends BasicTransportJSONObject implements JSONResponse {

    private Set<WallPost> wallMessages;

    private Set<WallDescription> wallUIs;

    public Set<WallPost> getWallMessages() {
        return wallMessages;
    }

    public void setWallMessages(Set<WallPost> wallMessages) {
        this.wallMessages = wallMessages;
    }

    public Set<WallDescription> getWallUIs() {
        return wallUIs;
    }

    public void setWallUIs(Set<WallDescription> wallUIs) {
        this.wallUIs = wallUIs;
    }
}
