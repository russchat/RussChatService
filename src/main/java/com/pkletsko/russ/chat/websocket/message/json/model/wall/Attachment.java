package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

public class Attachment extends BasicTransportJSONObject implements JSONResponse, Comparable {

    private Long attachmentMDBId;

    private Long attachmentId;

    private Long wallMessageId;

    private Long fromUserId;

    private String attachmentName;

    private String attachmentBaseURL;

    public Long getAttachmentMDBId() {
        return attachmentMDBId;
    }

    public void setAttachmentMDBId(Long attachmentMDBId) {
        this.attachmentMDBId = attachmentMDBId;
    }

    public String getAttachmentBaseURL() {
        return attachmentBaseURL;
    }

    public void setAttachmentBaseURL(String attachmentBaseURL) {
        this.attachmentBaseURL = attachmentBaseURL;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    @Override
    public int compareTo(Object o) {
        Attachment attachment = (Attachment) o;
        Long result = this.attachmentId - attachment.getAttachmentId();
        return result.intValue();
    }
}
