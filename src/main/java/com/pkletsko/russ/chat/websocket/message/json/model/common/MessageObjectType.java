package com.pkletsko.russ.chat.websocket.message.json.model.common;

import com.google.gson.annotations.SerializedName;

public enum MessageObjectType {

    @SerializedName("1")
    MOT_SEARCH(1),

    @SerializedName("2")
    MOT_CHAT_USER_SEARCH(2),

    @SerializedName("3")
    MOT_GET_UNRECEIVED_INFORMATION(3),

    @SerializedName("4")
    MOT_LIKE(4),

    @SerializedName("5")
    MOT_DISLIKE(5),

    @SerializedName("6")
    MOT_COMMENT(6),

    @SerializedName("7")
    MOT_UNRECEIVED_LIKES(7),

    @SerializedName("8")
    MOT_UNRECEIVED_COMMENTS(8),

    @SerializedName("9")
    MOT_LOGIN_STATUS(9),

    @SerializedName("10")
    MOT_NEW_MESSAGE(10),

    @SerializedName("11")
    MOT_NEW_WALL_MESSAGE(11),

    @SerializedName("12")
    MOT_CREATE_NEW_CONVERSATIONS(12),

    @SerializedName("13")
    MOT_RE_NEW_CONVERSATIONS(13),

    @SerializedName("14")
    MOT_RE_NEW_WALL(14),

    @SerializedName("15")
    MOT_LIST_OF_USERS(15),

    @SerializedName("16")
    MOT_LIST_OF_USER_PROFILE(16),

    @SerializedName("17")
    MOT_LIST_OF_CONVERSATIONS(17),

    @SerializedName("18")
    MOT_LIST_OF_CONVERSATION_MESSAGES(18),

    @SerializedName("19")
    MOT_CREATE_NEW_SCHOOL(19),

    @SerializedName("20")
    MOT_CREATE_NEW_SCHOOL_CLASS(20),

    @SerializedName("21")
    MOT_UPDATE_USER_PROFILE_INFO(21),

    @SerializedName("22")
    MOT_UNRECEIVED_MESSAGES(22),

    @SerializedName("23")
    MOT_FOLLOW_WALL(23),

    @SerializedName("24")
    MOT_UNFOLLOW_WALL(24),

    @SerializedName("25")
    MOT_UNRECEIVED_WALL_MESSAGES(25),

    @SerializedName("26")
    MOT_WALL_POSTS(26),

    @SerializedName("27")
    MOT_GET_USER_PROFILE(27),

    @SerializedName("28")
    MOT_SEND_COMPLETED_WALL_POST(28);

    private final int code;

    MessageObjectType(final int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }

    public static MessageObjectType findByCode(int code){
        for(MessageObjectType v : values()){
            if( v.getCode() == code){
                return v;
            }
        }
        return null;
    }
}
