package com.pkletsko.russ.chat.websocket.message.json.model.common;


/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
public class ImageDescription extends BasicTransportJSONObject implements JSONRequest {

    private String fileName;
    private String filePath;
    private String fileType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
