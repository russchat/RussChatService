package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class Like extends BasicTransportJSONObject implements JSONResponse, Comparable {

    private Long likeMDBId;

    private Long likeId;

    private Long fromUserId;

    private Long wallMessageId;

    private Boolean disabled;

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Long getLikeMDBId() {
        return likeMDBId;
    }

    public void setLikeMDBId(Long likeMDBId) {
        this.likeMDBId = likeMDBId;
    }

    public Long getLikeId() {
        return likeId;
    }

    public void setLikeId(Long likeId) {
        this.likeId = likeId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    @Override
    public int compareTo(Object o) {
        Like like = (Like) o;
        Long result = this.likeId - like.getLikeId();
        return result.intValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Like like = (Like) o;

        if (!likeId.equals(like.likeId)) return false;
        return wallMessageId.equals(like.wallMessageId);

    }

    @Override
    public int hashCode() {
        int result = likeId.hashCode();
        result = 31 * result + wallMessageId.hashCode();
        return result;
    }
}
