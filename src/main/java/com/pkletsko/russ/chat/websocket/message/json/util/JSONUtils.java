package com.pkletsko.russ.chat.websocket.message.json.util;

import com.google.gson.Gson;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.CreateNewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ReNewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.common.ImageDescription;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileEdit;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JSONUtils {

    static Log log = LogFactory.getLog(JSONUtils.class.getName());

    Gson gson = new Gson();

    public String getGSONbyResponseObject (JSONResponse responseObject) {
        return gson.toJson(responseObject);
    }

    public CreateNewChat getCreateNewConversationRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, CreateNewChat.class);
    }

    public ReNewChat getReNewConversationRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, ReNewChat.class);
    }

    public ChatMessage getChatMessage(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, ChatMessage.class);
    }

    public ImageDescription getImageDescription(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, ImageDescription.class);
    }

    public WallPost getWallMessageUI(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, WallPost.class);
    }

    public SendCompletedWallPostRequest getSendCompletedWallPostRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, SendCompletedWallPostRequest.class);
    }

    public SearchRequest getSearchRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, SearchRequest.class);
    }

    public UserProfileEdit getUserProfileEditRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, UserProfileEdit.class);
    }

    public SchoolInfo getSchoolInfoRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, SchoolInfo.class);
    }

    public SchoolClassInfo getSchoolClassInfoRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, SchoolClassInfo.class);
    }

    public Comment getComment(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, Comment.class);
    }

    public Like getLike(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, Like.class);
    }

    public UserProfileRequest getUserProfileRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, UserProfileRequest.class);
    }

    public FollowingChangeRequest getWallRequest(final String jsonFromServer) {
        return gson.fromJson(jsonFromServer, FollowingChangeRequest.class);
    }
}
