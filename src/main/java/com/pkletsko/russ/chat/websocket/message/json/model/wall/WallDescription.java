package com.pkletsko.russ.chat.websocket.message.json.model.wall;

import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 24.02.15
 * Time: 13:05
 * To change this template use File | Settings | File Templates.
 */
public class WallDescription {

    public Long wallMDBId;

    public Long wallId;

    public Integer newChatMessagesNumber;

    public Set<UserProfile> users;

    public String wallIcon;

    public Long getWallMDBId() {
        return wallMDBId;
    }

    public void setWallMDBId(Long wallMDBId) {
        this.wallMDBId = wallMDBId;
    }

    public Long getWallId() {
        return wallId;
    }

    public void setWallId(Long wallId) {
        this.wallId = wallId;
    }

    public Integer getNewChatMessagesNumber() {
        return newChatMessagesNumber;
    }

    public void setNewChatMessagesNumber(Integer newChatMessagesNumber) {
        this.newChatMessagesNumber = newChatMessagesNumber;
    }

    public Set<UserProfile> getUsers() {
        return users;
    }

    public void setUsers(Set<UserProfile> users) {
        this.users = users;
    }

    public String getWallIcon() {
        return wallIcon;
    }

    public void setWallIcon(String wallIcon) {
        this.wallIcon = wallIcon;
    }
}
