/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pkletsko.russ.chat.websocket.camel.processor.common;

import com.pkletsko.russ.chat.model.user.User;
import com.pkletsko.russ.chat.service.user.UserService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserLogin;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.util.JSONUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.atmosphere.websocket.WebsocketConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LOGIN_STATUS;

@Component("websocketSessionProcessor")
public class WebsocketSessionProcessor implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(WebsocketSessionProcessor.class);
    private static final String SRC_KEY = "src";
    private static final String TOKEN_KEY = "token";

    private JSONUtils jsonUtils = new JSONUtils();

    @Autowired
    private Global global;

    @Autowired
    private UserService userServices;

    public void process(Exchange exchange) throws Exception {

        Integer event = (Integer) exchange.getIn().getHeader(WebsocketConstants.EVENT_TYPE);
        String connectionKey = (String)exchange.getIn().getHeader(WebsocketConstants.CONNECTION_KEY);
        User user;


        switch (event) {
            case WebsocketConstants.ONOPEN_EVENT_TYPE:
                final String loginSrc = (String)exchange.getIn().getHeader(SRC_KEY);
                final String token = (String)exchange.getIn().getHeader(TOKEN_KEY);

                // generation of unique session security key
                final String internalToken = getNewToken();

                final UserProfile userProfile = userServices.login(token, loginSrc);
                user = userServices.getUserById(userProfile.getUserId());

                global.addUserWebsocketSessionKeyPair(connectionKey, user);

                //store token to memory with association with user Id
                global.addUserSessionKeyPair(userProfile.getUserId(), internalToken);

                global.addUserIdConnectionKeyPair(userProfile.getUserId(), connectionKey);

                //sendUserLoginResponse
                UserLogin userLoginResponse = new UserLogin();
                userLoginResponse.setLoginStatus("OK");
                userLoginResponse.setUserProfile(userProfile);
                userLoginResponse.setObjectType(MOT_LOGIN_STATUS);
                userLoginResponse.setToken(internalToken);

                final String json = jsonUtils.getGSONbyResponseObject(userLoginResponse);
                exchange.getIn().setBody(json);

                break;
            case WebsocketConstants.ONCLOSE_EVENT_TYPE:
                user = global.getUserWebsocketSessionKeyPair().get(connectionKey);
                global.removeUserWebsocketSessionKeyPair(connectionKey);
                global.removeUserSessionKeyPair(user.getUserId());
                global.removeUserIdConnectionKeyPair(user.getUserId());

                break;
            case WebsocketConstants.ONERROR_EVENT_TYPE:
                user = global.getUserWebsocketSessionKeyPair().get(connectionKey);
                global.removeUserWebsocketSessionKeyPair(connectionKey);
                global.removeUserSessionKeyPair(user.getUserId());
                global.removeUserIdConnectionKeyPair(user.getUserId());
                break;
        }
    }

    private String getNewToken() {
        return UUID.randomUUID().toString();
    }
}
