package com.pkletsko.russ.chat.websocket.camel.processor.util;

import org.apache.camel.Exchange;
import org.apache.camel.component.atmosphere.websocket.WebsocketConstants;
import org.slf4j.Logger;

import java.util.List;
import java.util.Set;

/**
 * Created by pkletsko on 09.12.2015.
 */
public class ProcessorUtil {
    public static final String RECIPIENTS_SET = "internal.recipients.set";

    public static void logging(Exchange exchange, String message, Logger log) {
        final String key = exchange.getIn().getHeader(WebsocketConstants.CONNECTION_KEY, String.class);
        log.debug("Sending To: " + key + ", " + message);
    }

    public static void sendWS(Exchange exchange, final String message) {
        sendWS(exchange, message, null);
    }

    public static void sendWS(Exchange exchange, final String message, List<String> connectionKeys) {
        if (connectionKeys != null) {
            exchange.getIn().setHeader(WebsocketConstants.CONNECTION_KEY_LIST, connectionKeys);
        }

        exchange.getIn().setBody(message);
    }

    public static void preSend(Exchange exchange, final String message, Set<Long> recipients, Long storedMessageId) {
        if (recipients != null) {
            exchange.getIn().setHeader(RECIPIENTS_SET, recipients);
        }

        if (storedMessageId != null) {
            exchange.getIn().setHeader(CamelConstants.MESSAGE_DB_ID, storedMessageId);
        }

        exchange.getIn().setBody(message);
    }

}
