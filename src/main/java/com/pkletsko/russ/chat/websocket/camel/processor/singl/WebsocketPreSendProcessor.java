/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pkletsko.russ.chat.websocket.camel.processor.singl;

import com.pkletsko.russ.chat.service.conversation.ConversationMessageService;
import com.pkletsko.russ.chat.service.wall.CommentHistoryService;
import com.pkletsko.russ.chat.service.wall.LikeHistoryService;
import com.pkletsko.russ.chat.service.wall.WallMessageService;
import com.pkletsko.russ.chat.websocket.camel.Global;
import com.pkletsko.russ.chat.websocket.camel.processor.util.CamelConstants;
import com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.atmosphere.websocket.WebsocketConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.pkletsko.russ.chat.websocket.camel.processor.util.CamelConstants.MESSAGE_TYPE;
import static com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil.logging;
import static com.pkletsko.russ.chat.websocket.camel.processor.util.ProcessorUtil.sendWS;

public class WebsocketPreSendProcessor implements Processor {
    private static final Logger log = LoggerFactory.getLogger(WebsocketPreSendProcessor.class);

    @Autowired
    private Global global;

    @Autowired
    private WallMessageService wallMessageServices;

    @Autowired
    private CommentHistoryService commentHistoryService;

    @Autowired
    private LikeHistoryService likeHistoryService;

    @Autowired
    private ConversationMessageService messageServices;

    public void process(Exchange exchange) throws Exception {
        Set<Long> userIds = exchange.getIn().getHeader(ProcessorUtil.RECIPIENTS_SET, Set.class);
        MessageObjectType messageType = MessageObjectType.findByCode(exchange.getIn().getHeader(MESSAGE_TYPE, Integer.class));
        Long messageId = exchange.getIn().getHeader(CamelConstants.MESSAGE_DB_ID, Long.class);
        String currentConnectionKey = (String)exchange.getIn().getHeader(WebsocketConstants.CONNECTION_KEY);
        String clientMessage = exchange.getIn().getBody(String.class);
        List<String> connectionKeys = new ArrayList<>();

        if (userIds == null) {
            userIds = new HashSet<>();
            userIds.add(global.getSessionUser(currentConnectionKey).getUserId());
        }

        for (final Long followerId : userIds) {
            String connectionKey = global.getUserIdConnectionKeyPair().get(followerId);

            if (connectionKey != null) {
                connectionKeys.add(connectionKey);
                //TODO  addReceived it should be generic for different messages

                log.debug("Sending Message To: " + connectionKey + ", " + clientMessage);

                switch (messageType) {
                    case MOT_COMMENT:
                        commentHistoryService.addReceived(messageId, followerId);
                        commentHistoryService.removeUnReceived(messageId, followerId);
                        break;
                    case MOT_CREATE_NEW_CONVERSATIONS:
                        //conversationServices... missing
                        break;
                    case MOT_LIKE:
                        likeHistoryService.addReceived(messageId, followerId);
                        likeHistoryService.removeUnReceived(messageId, followerId);
                        break;
                    case MOT_DISLIKE:
                        likeHistoryService.addReceived(messageId, followerId);
                        likeHistoryService.removeUnReceived(messageId, followerId);
                        break;
                    case MOT_NEW_MESSAGE:
                        messageServices.addReceived(messageId, followerId);
                        break;
                    case MOT_NEW_WALL_MESSAGE:
                        wallMessageServices.addReceived(messageId, followerId);
                        wallMessageServices.removeUnReceived(messageId, followerId);
                        break;
                    case MOT_SEND_COMPLETED_WALL_POST:
                        wallMessageServices.addReceived(messageId, followerId);
                        wallMessageServices.removeUnReceived(messageId, followerId);
                        break;
                }
            }
        }

        logging(exchange, clientMessage, log);
        sendWS(exchange, clientMessage, connectionKeys);
    }
}
