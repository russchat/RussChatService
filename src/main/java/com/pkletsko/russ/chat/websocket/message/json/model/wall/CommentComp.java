package com.pkletsko.russ.chat.websocket.message.json.model.wall;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class CommentComp implements Comparator<Comment> {
    @Override
    public int compare(Comment e1, Comment e2) {
        final long id1 = e1.getCommentId(), id2 = e2.getCommentId();
        if (id1 == id2)
            return 0;
        return id1 < id2 ? 1 : -1;
    }
}
